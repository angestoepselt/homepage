PROJECT_DIR=$(dirname "$(dirname "$0")")
NIX_DIR="$PROJECT_DIR/nix"

if [ -h "$PROJECT_DIR/node_modules" ]; then
	rm node_modules
fi

npm install --package-lock-only
node2nix \
	-i "$PROJECT_DIR/package.json" \
	-l "$PROJECT_DIR/package-lock.json" \
	-o "$NIX_DIR/node-packages.nix" \
	-c "$NIX_DIR/default.nix" \
	-e "$NIX_DIR/node-env.nix" \
	--development \
	--include-peer-dependencies

nix build -o "$PROJECT_DIR/.dev" ".#devEnv"

if [ ! -e "$PROJECT_DIR/node_modules" ]; then
	cd "$PROJECT_DIR"; ln -s .dev/lib/node_modules .
fi
