{
  description = "Angestöpselt Homepage";

  inputs.nixpkgs.url = "nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {inherit system;};

      nodejs = pkgs.nodejs-18_x;
      nodePackages = import ./nix/default.nix {inherit pkgs system nodejs;};
      nodeDependencies = nodePackages.nodeDependencies.override {
        src = pkgs.runCommand "angestoepselt-homepage-package-json" {} ''
          mkdir -p "$out"
          cp ${./package.json} "$out/package.json"
          cp ${./package-lock.json} "$out/package-lock.json"
        '';
        nativeBuildInputs = [pkgs.pkg-config];
        buildInputs = [pkgs.vips pkgs.glib];
      };

      python = pkgs.python310.withPackages (ps:
        with ps; [
          itsdangerous
          requests
        ]);
    in rec {
      packages = {
        devEnv = pkgs.symlinkJoin {
          name = "angestoepselt-homepage-dev";
          paths = [
            pkgs.lighttpd
            nodejs
            nodeDependencies
            python
          ];

          buildInputs = [pkgs.makeWrapper];
          postBuild = ''
            wrapProgram "$out/bin/node" \
            	--prefix PATH : "$out/lib/node_modules/.bin" \
            	--prefix NODE_PATH : "$out/lib/node_modules"
          '';

          shellHook = ''
            echo ""
            echo "  To start editing content, run:"
            echo ""
            echo "npm run build:styles"
            echo "npm run dev:site"
            echo ""
            echo "  The site will be available under http://localhost:8080/ for"
            echo "  local development and rebuilds automatically when content"
            echo "  changes."
            echo ""
          '';
        };
      };

      devShells.default = pkgs.stdenvNoCC.mkDerivation {
        name = "angestoepselt-homepage-shell";
        nativeBuildInputs = [packages.devEnv];
      };
    });
}
