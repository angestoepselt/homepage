#
# Static site build phase
#
FROM node:16 as build
ARG SITE

WORKDIR /build

COPY package.json package-lock.json /build/
RUN npm ci

COPY styles /build/styles/
RUN npm run build:styles

COPY .eleventy* /build/
COPY assets /build/assets/
COPY includes /build/includes/
COPY sites/${SITE} /build/sites/${SITE}/
# These are symlinked from other sites:
COPY sites/angestoepselt/_images/home-banner.jpg /build/sites/angestoepselt/_images/

RUN SITE=${SITE} npm run build


#
# Actual server container
#
FROM python:3.10-alpine
ARG SITE

# Install dependencies, see flake.nix for details.
RUN apk add --no-cache lighttpd && \
	python -m pip install itsdangerous requests

COPY --from=build /build/dist /www/
COPY cgi-bin /cgi-bin/
COPY sites/${SITE}/httpd.conf /httpd.conf
COPY sites/${SITE}/_data/config.json /config.json

# Patch the lighttpd config file. These placeholders expect a Nix derivation
# path, so the Python binary used in the end is at @python@/bin/python. The
# values here make sure they are found correctly in the non-Nix container,
# even though the might not necessarily make sense directly.
RUN sed -i \
	-e "s,@mimetypes@,/etc/lighttpd/mime-types.conf,g" \
	-e "s,@python@,/usr/local,g" \
	-e  "s,@site@,/www,g" \
	-e  "s,@cgibin@,/cgi-bin,g" \
	/httpd.conf

EXPOSE 80/tcp
# Note: these are just those environment variables that have a default variable
# and are optional, the non optional ones *will* need to be set by the user.
ENV ZAMMAD_URL=https://ticket.z31.it
ENV ZAMMAD_GROUP=""

HEALTHCHECK --interval=30s --timeout=10s --retries=3 \
	CMD wget --no-verbose --tries=1 --spider http://127.0.0.1/ || exit 1

# Reroute stdout to a new file descriptor so that Lighttpd can use it as the
# access log. See http.conf and here:
# https://redmine.lighttpd.net/boards/2/topics/8382
ENTRYPOINT exec 3>&1 && /usr/sbin/lighttpd -Df /httpd.conf
