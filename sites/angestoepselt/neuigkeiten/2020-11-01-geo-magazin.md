---
title: GEO Magazin berichtet über uns
date: 2020-11-01
source: GEO Magazin
blurb:
  'Für den Verein »Angestöpselt« setzt eine Gruppe von Nerds 
  alte Computer wieder instand – und verschenkt sie an Arme'
layout: post
---

Im Geo Magazin wird über unsere ehrenamtliche Tätigkeit berichtet. 
Den kompletten Beitrag kann man in der Ausgabe 11/2020 nachlesen:


> [![GEO Magazin 2020/11](/assets/magazine/geo-11-2020-zukunft-cover.png)](https://www.geo.de/wissen/23500-rtkl-hobby-und-hilfsprogramm-wie-ein-verein-aus-nerds-beduerftigen-zu-computern){target="_blank" rel="noopener noreferrer"}
