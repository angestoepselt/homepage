---
title: "Run auf angestöpselt"
date: 2017-04-17
source: Main-Post
sourceLogo: mainpost.gif
blurb:
  'Großer Run auf die Ware von angestöpselt'
layout: post
---

Im Vereinsladen angestöpselt gibt es gespendete Computer für wenig Geld. Vor allem Flüchtlinge sind dankbar für die wieder
flott gemachten gebrauchten Geräte. „Meine Güte, der Drucker ist ja quasi neu!“ Moritz Beck von der Computerspende Würzburg 
ist beeindruckt. Eine lokale Firma übergab dem Verein nun schon zum zweiten Mal innerhalb weniger Wochen ausrangierte
elektronische Geräte. 200 Notebooks, 70 Drucker und verschiedene Netzteile befinden sich in den 25 Kartons, die der 17-
Jährige zusammen mit Vereinskollegen heute in drei Fuhren von dem Unternehmen zum Vereinsladen angestöpselt in die Zellerau geschafft hat.

### Auf Funktionstüchtigkeit geprüft 
In den kommenden Wochen wird jedes Gerät auf seine Funktionstüchtigkeit geprüft, sämtliche Daten werden von den
Festplatten gelöscht, außerdem spielt das angestöpselt-Team auf jedes Notebook das Betriebssystem Linux auf. „Dafür
werden wir eineinhalb Monate brauchen“, schätzt Florian Helmerich, der sich zusammen mit Moritz Beck im Vorstand des
„Vereins für Digitalkompetenz“, dem Träger von angestöpselt, engagiert. Vor ihm stehen vier aufeinander gestapelte
Notebooks, deren Daten gerade gelöscht werden. Vor eineinhalb Stunden startete der Prozess: „Das wird jetzt wohl noch
eine halbe Stunde dauern.“

### Der Ansturm ist groß 

Abnehmer für die Notebooks gibt es reichlich. „Wir erlebten in den letzten Wochen einen regelrechten Ansturm“, sagt
Helmerich. An den Öffnungsabenden, Montag und Mittwoch von 18.15 bis 20.20 Uhr, kamen stets um die 30 Menschen in
den Laden in der Frankfurter Straße 74. Keiner von ihnen hatte genug Geld, um sich einen Rechner im Geschäft kaufen zu
können. Der Ansturm war so groß, dass erstmals Nummern ausgegeben werden mussten.
Laut Helmerich fragen momentan in erster Linie Flüchtlinge nach einem Computer. „Sie benötigen ihn, um Kontakt in ihr
Heimatland zu halten“, so der 17-jährige Realschüler, der im August eine Lehre zum Fachinformatiker beginnen wird. Nach
wie vor kommen aber auch Ruheständler mit Minirente sowie Menschen im Hartz IV-Bezug, für die selbst gebrauchte 50-
Euro-Computer, wie sie über das Internet angeboten werden, unerschwinglich wären.

### Flüchtlinge, Rentner, Hartz IV-Bezieher

Dass es kaum möglich ist, vom Regelsatz so viel Geld abzuknapsen, um sich dafür einen Rechner zu kaufen, bestätigt Gina
H. (Name geändert). Die 55-jährige Würzburgerin lebt seit 2009 auf Hartz IV-Niveau. Ein Burnout, erzählt die angestöpselt-
Kundin, katapultierte sie damals aus dem Erwerbsleben. 2011, als die Computerspende Würzburg gegründet wurde, suchte
sie das Team gleich auf, um sich einen Rechner zu organisieren. Mit diesem inzwischen mehr als zehn Jahre alten Gerät
kommt sie heute zu Moritz Beck. Bisher habe sie das Notebook vor allem zum Schreiben benutzt, erzählt sie: „Jetzt möchte
ich ins Internet gehen, aber das klappt irgendwie nicht.“
Der 17-jährige Gymnasiast stellt fest, dass mit dem Notebook selbst alles okay ist. Allerdings hat Gina H. Probleme mit dem
Betriebssystem Ubuntu, das ihr ein Bekannter auf den Rechner gespielt hat. Überhaupt würde sich die
Sozialhilfeempfängerin gern fitter machen fürs Recherchieren im Netz. Denn sie hat neue berufliche Perspektiven. Beck
verweist sie auf die Infoabende, die zwei Ehrenamtliche aus dem angestöp-selt-Team regelmäßig anbieten: „Da erfahren
Sie alles darüber, wie man ins Internet kommt, wie man eine Mail schreibt und surft.“

### „Verein für Digitalkompetenz“

40 Menschen gehören dem „Verein für Digitalkompetenz“ derzeit an. Rund ein Dutzend Freiwilliger engagiert sich in der
Werkstatt, repariert Computer, holt gebrauchte Rechner ab, bedient Kunden oder organisiert Veranstaltungen. Geld für
Miete und Equipment fließt dem Verein aus Mitgliedsbeiträgen und Spenden zu. Seit Jahresbeginn wird außerdem eine
Bearbeitungsgebühr von zehn Euro pro Rechner erhoben.

Dies wurde laut Helmerich notwendig, weil der Verein vor einem haben Jahr einen finanziellen Engpass hatte. Die Kunden
akzeptierten die Umstellung, ohne zu murren. Die Rechner bleiben ja dennoch unschlagbar günstig.

Trotz der großzügigen PC-Spende des Würzburger Unternehmens ist der Verein aufgrund der hohen Nachfrage weiter auf
der Suche nach gebrauchten Computern. Ende Juni wird bei der Mitgliederversammlung außerdem über einen möglichen
Umzug diskutiert. Rein aus Platzgründen, so Helmerich, wäre dies sinnvoll, denn die Werkstatt platzt aus allen Nähten.
Fragt sich nur, ob es gelingt, günstige Räume in Würzburg zu finden.

Wer das Team von angestöpselt kennen lernen möchte, hat dazu auf dem Umsonst & Draussen Festival vom 15. bis 18.
Juni Gelegenheit

> [Artikel als PDF herunterladen](/assets/magazine/run_auf_angestoepselt.pdf){target="_blank"}
