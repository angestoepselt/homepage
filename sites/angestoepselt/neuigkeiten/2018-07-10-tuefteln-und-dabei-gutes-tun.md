---
title: "Tüfteln und dabei Gutes tun"
date: 2017-07-10
source: Main-Post
sourceLogo: mainpost.gif
url: https://www.mainpost.de/regional/wuerzburg/tuefteln-und-dabei-gutes-tun-art-10006292
blurb:
  'Seit Februar hat der Verein angestöpselt in der Zeller Str. 29/31
  eine neue Heimat. Wir haben uns von 40qm auf 100qm verdoppelt.'
layout: post
---
