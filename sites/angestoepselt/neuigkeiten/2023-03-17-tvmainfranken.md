---
title: TV Mainfranken hat uns besucht - jetzt in der Mediathek verfügbar
date: 2023-03-17
source: TV Mainfranken
sourceLogo: tvmainfranken.svg
blurb:
  'Leben wir in einer Wegwerfgesellschaft?'
layout: post
---

Der lokale Fernsehsender [TV Mainfranken](https://www.tvmainfranken.de/){target="_blank" rel="noopener noreferrer"} war zu Besuch bei uns im Verein und berichtet über unsere Tätigkeit. Neben uns kommt auch noch das Team des [Luftschloss](https://umsonstladen4wuerzburg.wordpress.com/){target="_blank" rel="noopener noreferrer"} und der [Brauchbar](https://www.brauchbarggmbh.de/){target="_blank" rel="noopener noreferrer"} zu Wort.

Das Video findet ihr [hier in der Mediathek.](https://www.tvmainfranken.de/mediathek/video/leben-wir-in-einer-wegwerfgesellschaft-einrichtungen-wollen-dagegen-ankaempfen/){target="_blank" rel="noopener noreferrer"} 
