---
title: Computer sind mein Leben
date: 2017-04-17
source: Publik Forum
sourceLogo: publik-forum.png
url: https://www.publik-forum.de/Publik-Forum-09-2017/computer-sind-mein-leben
blurb:
  'Der 17 Jahre alte Gymnasiast Moritz Beck macht alte
  Rechner wieder fit für Menschen mit kleinem Geldbeutel.'
layout: post
---
