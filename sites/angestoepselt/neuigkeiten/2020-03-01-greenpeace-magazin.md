---
title: Wir sind „Wegweiser“ im Greenpeace Magazin
date: 2020-03-01
source: Greenpeace Magazin
blurb:
  'Gestern noch Schrott, heute wieder flott'
layout: post
---

Die Medien sind auf uns aufmerksame geworden und wollen einen Artikel in 
der Kategorie "Wegweiser" über uns veröffentlichen. Den kompletten Beitrag
könnt Ihr auf der Homepage von Greenpeace nachlesen:

> [![Greenpeace Magazin](/assets/magazine/GP03-2020_Titel_RGB_96dpi.jpg)](https://www.greenpeace-magazin.de/wegweiser/matthias-hemmerich-lukas-seeber){target="_blank" rel="noopener noreferrer"}
