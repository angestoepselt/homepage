---
title: Hardwarespende - iWelt spendet iMacs
date: 2023-03-31
source: angestoepselt
sourceLogo: angestoepselt.png
blurb:
  'iWelt spendet Hardware für Empfänger:innen mit speziellen Anforderungen'
layout: post
---

Die [iWelt](https://www.iwelt.de/){target="_blank" rel="noopener noreferrer"} aus Eibelstadt bietet profesionelle IT-Solutions für Unternehmen, für uns spenden sie profesionelle Hardware. 
Heute haben wir ein ganzes Auto voll mit feinsten iMacs bekommen. Sie sollen insbesondere unseren Empfänger:innen zu Gute kommen, die spezielle Ansprüche an ihre Hardware haben. 

Ihr studiert Grafikdesign, Architektur oder braucht aus einem anderen Grund ein schnelles Gerät? Schreibt uns im [Kontaktformular](/computer-beantragen/) (ab Mai nehmen wir neue Anfragen an) was ihr braucht.

Danke an die iWelt für die großzügige Spende und auch dafür, dass sie uns als Partner bei den [CoderDojos](https://coderdojo-wue.de/){target="_blank" rel="noopener noreferrer"} supportet.

![Das Bild zeigt den offenen Kofferraum eines Autos, der mit knapp zehn großen Rechnern von Apple beladen ist. Die Geräte sind gepolstert und fertig zum Transport.](/assets/magazine/20230331_iWelt_iMacs.jpg){target="_blank" width=60%}
