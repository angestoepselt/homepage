---
title: Ein neuer Anstrich
date: 2021-01-01
blurb:
  'Angestöpselt bekommt ein neues Auftreten: frisches Logo, erneuerte Homepage
  und einfachere Kontaktmöglichkeiten!'
layout: post
---

Lange hat's gedauert – jetzt endlich freuen wir uns, unser neues
Vereins-"Branding" präsentieren zu können. Dazu zu gehört:

- Ein eigens entworfenes Logo samt Schriftzug
- Diese neue Homepage
- Diverse Flyer
- Marketing-Kram wie Tassen

Zum einen möchten wir damit unserem Verein ein neues, modernes Auftreten 
verpassen. Des Weiteren sind im Hintergrund auch ein paar technische 
Veränderungen erfolgt. Dadurch können wir in Zukunft Anfragen besser sammeln, 
strukturieren und abarbeiten. Gleichzeitig versuchen wir es allen BesucherInnen 
der Homepage möglichst einfach zu machen, mit uns in Kontakt zu treten. Weniger 
Hürden tragen letztendlich auch dazu bei, unsere Projekte an mehr Menschen zu 
bringen. Dass das eine gute Sache ist, davon sind wir überzeugt!
