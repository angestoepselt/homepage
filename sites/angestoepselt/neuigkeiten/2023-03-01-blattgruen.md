---
title: Das Magazin BlattGrün berichtet über uns in der aktuellen Ausgabe
date: 2023-03-01
source: BlattGrün
blurb:
  'Verein Angestöpselt bereitet alte Computer auf.'
layout: post
---

Das Magazin [BlattGrün](https://blatt-atelier.de/){target="_blank" rel="noopener noreferrer"} berichtet über unsere ehrenamtliche Arbeit in der aktuellen Ausgabe. 
Den Beitrag könnt ihr unten nachlesen. 


> [![BlattGrün Magazin 2023/2](/assets/magazine/2023_03_01_blattgruen.PNG)](https://blatt-atelier.de/wp-content/uploads/2023/03/Blattgruen-02-23-screen-1.pdf){target="_blank" rel="noopener noreferrer"}

Die komplette Ausgabe findet ihr [hier](https://blatt-atelier.de/wp-content/uploads/2023/03/Blattgruen-02-23-screen-1.pdf){target="_blank" rel="noopener noreferrer"}
