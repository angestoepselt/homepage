---
title: Umsonst & Draußen 2022
date: 2022-06-12
blurb:
  'Auch dieses Jahr sind wir wieder auf dem Umsonst und Draußen Festival'
layout: post
---

Endlich kommt das Umsonst und Draußen Festival wieder zurück auf die Mainwiesen. Natürlich ist auch Angestöpselt e. V. wieder mit dabei. Dieses Jahr könnt ihr bei uns:
- einen Blick auf die neue Homepage werfen.
- Hardware zerlegen und einzelne Komponenten suchen.
- der Nerdseele freien Lauf lassen.

Ihr findet uns im hinteren Teil des Festivals bei den Initiativen. Wir freuen uns auf euren Besuch.