---
title: Riesiger Ansturm auf alte PCs
date: 2017-06-22
source: Südwest-Presse
sourceLogo: suedwest.png
url: /assets/magazine/Artikel_Suedwest.pdf
blurb:
  'Verein für Digitalkompetenz Würzburg: Lokale Firma übergab 
  200 Notebooks als Spende / Dankbare Abnehmer'
layout: post
---
