---
title: NerdHorizons - neues monatliches Vereinstreffen
date: 2023-03-26
source: NerdHorizons
sourceLogo: angestoepselt.png
blurb:
  'Einführung in den 3D Druck'
layout: post
---

Im Dezember 2022 haben wir ein neues monatliches Vereinstreffen ins Leben gerufen. Das Treffen läuft unter dem Titel "NerdHorizons". 
Nachdem wir im letzten Treffen im Februar 2023 eine Einführung in [git](https://de.wikipedia.org/wiki/Git){target="_blank" rel="noopener noreferrer"} durch unsere Vereinsmitglieder Yannik und Maximillian bekommen haben, stand unser Vereinsabend im März unter dem Titel "Einführung in den 3D Druck".

![Schulung im 3D Druck](/assets/magazine/nerdhorizons_3ddruck.jpg){target="_blank" width=100%}

Tobias hat uns unter anderem die verschiedenen Druckverfahren, Vor- und Nachteile und Anwendungsgebiete des 3D Drucks näher gebracht. Anschließend konnte der Drucker seine Fähigkeiten beim Drucken einer [Pfeife](https://www.thingiverse.com/thing:1179160){target="_blank" rel="noopener noreferrer"} vorführen.

![3D gedruckte Pfeife](/assets/magazine/nerdhorizons_3ddruck_2.jpg){target="_blank" width=40%}

Falls ihr Interesse an einer Mitarbeit bei uns habt, meldet euch unter [Kontakt](https://angestoepselt.de/mitmachen).
