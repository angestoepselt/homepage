---
title: Studierende fördern junge Programmierer
date: 2017-12-18
source: Main-Post
sourceLogo: mainpost.gif
blurb:
  'Der Verein angestöpselt e.V. veranstaltet CoderDojos und fördert
  junger Programmierer'
layout: post
---

![Beitrag in der Main-Post: 20 Master-Studierende boten an der Hochschule Würzburg-Schweinfurt ein sogennantes "CoderDojo" an, einen kostenlosen Programmier-Workshop für knapp 40 interessierte Kinder sowie begleitende Eltern. Die Studierenden der Fakultät Informatik und Wirtschaftsinformatik betreuten Kinder und Jugendliche im Alter von neun bis 17 Jahren und halfen ihnen bei verschiedenen Programmierprojekten – sie konnten sich in der Anfänger- oder Fortgeschrittenengruppe anschließen, sich der Java-Programmierung widmen, in der Kreativgruppe Webseiten designen, bei der Hardware-Bastelgruppe mitmachen – je nach Interessensgebiet und Kenntnissen. Den Teilnehmern wurde neben professioneller Hilfe und Unterstützung ihrer eigenen Ideen zwei Vorträge angeboten zu den Themen Eye-Tracking und IT-Security. Zum Abschluss der Veranstaltung konnten die Schülerinnen und Schüler ihre Projekte vorstellen und neue Ideen einbringen für den nächsten CoderDojo an der FHWS. Das Angebot der CoderDojos gibt es weltweit. Unterstützt werden sie global von der CoderDojo Foundation aus Irland, lokal vom Verein "angestöpselt e.V.".](/assets/magazine/MP-2017-12-18-Coder-Dojo.jpeg){target="_blank" width=100%}
