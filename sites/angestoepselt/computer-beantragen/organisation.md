---
layout: layouts/page.njk
useForms: true
---

# Geräte für eine gemeinnützige Organisation beantragen

<form method="post" action="/computer-beantragen/organisation">

Bitte beachte, dass die Hardware in Würzburg abgeholt werden muss. Wir bieten keinen Versand an.
Wir möchten nicht, dass gute Aktionen an fehlender Hardware scheitern. Deshalb
haben gemeinnützige Organisationen wie Vereine die Möglichkeit, Hardware für
ihre Zwecke zu beantragen. Dazu benötigen wir zunächst deine Kontaktdaten als
Ansprechpartner.

<label class="form-input">
  <span>Name:</span>
  <input type="text" name="contactname" required placeholder="Vorname Nachname" />
</label>

<label class="form-input">
  <span>Email:</span>
  <input type="email" name="contactemail" required placeholder="mail@beispiel.de" />
</label>

In den folgenden Feldern kannst du uns mitteilen, um welchen Empfänger es sich
handelt. Hier geht es um die Organisation, nicht um konkrete Mitarbeiterinnen
oder Mitarbeiter.

<label class="form-input">
  <span>Name:</span>
  <input type="text" name="organization" required placeholder="Name der Organisation" />
</label>

<label class="form-input">
  <span>Adresse:</span>
  <input type="text" name="addressline" required placeholder="Straße und Hausnummer" />
</label>

<label class="form-input">
  <span>Postleitzahl:</span>
  <input type="text" name="postalcode" required placeholder="12345" />
</label>

<label class="form-input">
  <span>Ort:</span>
  <input type="text" name="city" required placeholder="Würzburg" />
</label>

Nun würden wir gerne von dir eine grobe Auflistung der Geräte erfassen, die ihr
benötigt. Genauere Anforderungen kannst du auch weiter unten im Freitextfeld
angeben.

<label class="form-input">
  <span>Anzahl Desktops:</span>
  <input type="number" name="desktopcount" value="0" min="0" max="100" />
</label>

<label class="form-input">
  <span>Anzahl Laptops:</span>
  <input type="number" name="laptopcount" value="0" min="0" max="100" />
</label>

<label class="form-input">
  <span>Anzahl Drucker:</span>
  <input type="number" name="printercount" value="0" min="0" max="100" />
</label>

Im folgenden Feld kannst du weitere Anmerkungen ergänzen.

<label class="form-input">
  <span>Eigene Angaben:</span>
  <textarea name="message"></textarea>
</label>

<!-- FORM -->

<div class="form-submit">
  <input type="submit" value="Abschicken" />
</div>

</form>
