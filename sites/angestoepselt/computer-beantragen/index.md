---
layout: layouts/page.njk
useForms: true
eleventyNavigation:
  key: Computer beantragen
  order: 50
---

# Computer beantragen

Damit wir dir einen Computer geben können, musst du uns einige Fragen beantworten.
Danach können wir den Antrag prüfen und uns bei dir melden.


Für wen brauchst du den Computer?

- [_Privat_ für mich oder meine Familie](/computer-beantragen/privat)
- [Für eine _gemeinnützige Organisation_](/computer-beantragen/organisation)
  {.form-choices}

Alle Daten werden natürlich [geschützt verarbeitet](/datenschutzhinweis).

## Informationen und häufig gestellte Fragen

Es ist sehr einfach, einen Computer von uns zu erhalten. Normalerweise läuft es
in drei Schritten ab:

1. Du stellst einen Antrag über das Kontaktformular.
   ([oben](#computer-beantragen) auf dieser Seite)
2. Wir vereinbaren einen Termin zur Abholung mit dir. Falls kein Gerät direkt
   zur Verfügung steht, setzen wir dich auf unsere Warteliste.
3. Du erhältst zum vereinbarten Termin einen Computer von uns.

### Wer bekommt einen Computer?

Alle Personen, die einen der folgenden Nachweise vorlegen können:

- eine Arbeitslosengeldbescheinigung (ALG I)
- Bescheinigung über Bürgergeld (früher ALG II / Hartz IV)
- ein Bewilligungsbescheid (für Geflüchtete)
- Bescheinigungen vom Sozialamt, von der Schuldnerberatung oder vom Jobcenter
- Bescheinigung eines anerkannten Trägers (z. B. Caritas, Diakonie, Jugendhilfeeinrichtungen)
- BAföG-Bescheid
- Rentenbescheid
- Grundsicherungsbescheid
- Lohnbescheid (maximal der 5-fache Regelsatz)

Außerdem verschenken wir PCs auch an Organisationen, die selbst gemeinnützige
oder mildtätige Zwecke erfüllen. Nimm in diesem Fall gerne über den Link [oben](#computer-beantragen)
Kontakt zu uns auf, dann finden wir gemeinsam eine passende Lösung.

### Wie stelle ich einen Antrag?

Über die beiden Links [oben](#computer-beantragen) auf dieser Seite gelangst du
zum entsprechenden Kontaktformular. Bitte benutze dieses, um einen Computer zu
beantragen.

### Wie lange dauert es?

Alle Anfragen bearbeiten wir der Reihe nach. Wenn gerade viele Anträge bei uns
eintreffen, kann die Bearbeitungszeit durchaus ein paar Monate dauern. In
ruhigeren Zeiten können wir dir oft auch schon in zwei Wochen einen Computer
überreichen. In jedem Fall wirst du von uns benachrichtigt, sobald ein Gerät für
dich bereitsteht.
Allgemein gilt: Computer (Desktop PCs) gehen schnell. Laptops dauern länger (aktuell mindestens sechs Monate).

_Hinweis: da unsere Arbeit von Ehrenamtlichen gestemmt wird – also Freiwilligen,
die kein Geld dafür erhalten – geben wir bei diesem Thema keine festen
Versprechen._

### Versendet ihr Computer?

Nein.

### Warum versendet ihr keine Computer?

Wir sind nur wenige Freiwillige, die an dem Projekt arbeiten. Aktuell haben wir
keine Kapazitäten, Computer per Post zu verschicken.

### Gibt es eine Garantie auf die Geräte?

Nein. Es sind gebrauchte Computer, auf die wir keine Garantie geben.

### Welches Betriebssystem ist auf den Computern installiert?

Auf allen unseren Geräten ist [Linux Mint](https://www.linuxmint.com/)
installiert. Es gibt ein Programm zum Schreiben
([LibreOffice](https://de.libreoffice.org/)) und für das Internet
([Firefox](https://www.mozilla.org/de/firefox/new/)). Außerdem gibt es viele
weitere kostenlose Software.
