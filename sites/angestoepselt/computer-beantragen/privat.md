---
layout: layouts/page.njk
useForms: true
---

# Privat einen Computer beantragen

{% if config.applicationsClosed %}

> Aktuell haben wir keine Hardware verfügbar.
> Bitte schaue in ein paar Wochen wieder auf unserer Homepage vorbei.
> 
> Danke für dein Verständnis!

{% endif %}



<form method="post" action="/computer-beantragen/privat" enctype="multipart/form-data">
{% if config.applicationsClosed %}
  <fieldset disabled="disabled" class="blanked-out-form">
{% endif %}
Auf dieser Seite kannst du einen Antrag einreichen, um einen Computer von uns zu
erhalten. Bitte teile uns zunächst deine Kontaktdaten mit.

<label class="form-input">
  <span>Name:</span>
  <input type="text" name="contactname" required placeholder="Vorname Nachname" />
</label>

<label class="form-input">
  <span>Email:</span>
  <input type="email" name="contactemail" required placeholder="mail@beispiel.de" />
</label>

Welches Gerät würdest du gerne von uns erhalten?

{% for device in config.devices %}

<input type="radio" id="hardware-{{ loop.index }}" class="radio-input" name="hardware" required value="{{ device.name }}" {% if not device.available %}disabled{% endif %}/>
<div class="form-input">
	<label for="hardware-{{ loop.index }}">{{ device.name }}{% if not device.available %}<br><em>Aktuell nicht verfügbar</em>{% endif %}</label>
</div>

{% endfor %}


Damit du einen Computer von uns erhalten kannst, benötigen wir von dir einen
Nachweis der Bedürftigkeit. Das ist eines dieser Papiere, die von offiziellen
Stellen ausgestellt werden:

- eine Arbeitslosengeldbescheinigung (ALG I)
- Bescheinigung über Bürgergeld
- ein Bewilligungsbescheid (für Geflüchtete)
- Bescheinigungen vom Sozialamt, von der Schuldnerberatung oder vom Jobcenter
- Bescheinigung eines anerkannten Trägers (z. B. Caritas, Diakonie, Jugendhilfeeinrichtungen)
- BAföG-Bescheid
- Rentenbescheid
- Grundsicherungsbescheid
- Lohnbescheid (maximal der 5-fache Regelsatz)

Bitte lade das Dokument im folgenden Schritt hoch – entweder als Foto oder
eingescannt als PDF.

<label class="form-input">
  <span>Nachweis hochladen:</span>
  <input type="file" name="document" required />
</label>

Bitte gib uns jetzt noch deine Anschrift. Das sollte die gleiche sein, die auch
im obigen Dokument erwähnt ist.

<label class="form-input">
  <span>Adresse:</span>
  <input type="text" name="addressline" required placeholder="Straße und Hausnummer" />
</label>

<label class="form-input">
  <span>Postleitzahl:</span>
  <input type="text" name="postalcode" required placeholder="12345" />
</label>

<label class="form-input">
  <span>Ort:</span>
  <input type="text" name="city" required placeholder="Würzburg" />
</label>

Danke für deine Angaben. Möchtest du uns noch etwas mitteilen?

<label class="form-input">
  <span>Eigene Angaben:</span>
  <textarea name="message"></textarea>
</label>

Bevor du das Formular abschickst, lies dir bitte die folgenden Hinweise
aufmerksam durch:

- Wir können leider nur Anfragen aus Unterfranken annehmen. Du kannst einen Computer nur bekommen, wenn du in [Unterfranken](https://de.wikipedia.org/wiki/Unterfranken){target="_blank" rel="noopener noreferrer"} lebst.
- Wir versenden keine Geräte per Post – der Computer muss persönlich abgeholt
  werden.
- Die aktuelle geschätzte Wartezeit liegt bei 2 – 6 Wochen.
- Auf allen unseren Geräten ist [Linux Mint](https://www.linuxmint.com/){target="_blank" rel="noopener noreferrer"}
installiert. Es gibt ein Programm zum Schreiben
([LibreOffice](https://de.libreoffice.org/){target="_blank" rel="noopener noreferrer"}) und für das Internet
([Firefox](https://www.mozilla.org/de/firefox/new/){target="_blank" rel="noopener noreferrer"}).

<label class="form-checkbox">
  <input type="checkbox" name="hints" required>
  <div></div>
  <span>Ich habe die Hinweise gelesen.</span>
</label>

<!-- FORM -->

<div class="form-submit">
  <input type="submit" value="Abschicken" />
</div>

{% if config.applicationsClosed %}
  </fieldset>
{% endif %}
</form>
