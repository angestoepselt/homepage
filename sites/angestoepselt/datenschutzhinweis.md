---
layout: page
tags:
  - legal
eleventyNavigation:
  key: Datenschutz
  order: 30
---

# Datenschutzhinweis

### 1. Name und Kontaktdaten des für die Verarbeitung Verantwortlichen

Diese Datenschutz-Information gilt für die Datenverarbeitung durch:  
angestöpselt e.V.  
Zeller Str. 29/31  
97082 Würzburg  
E-Mail: datenschutz@angestoepselt.de

Die Bestellung eines Datenschutzbeauftragten ist nicht erforderlich.

## 2. Erhebung und weitere Verarbeitung personenbezogener Daten inkl. Art und Zweck der Verwendung

### 2.1 Beim Besuch der Webseite

Wenn Sie meine Webseite besuchen, übermittelt Ihr Browser systembedingt gewisse Informationen an den Server, die Sie wiederum identifizierbar machen könnten. So übermitteln Sie bzw. Ihr Webbrowser z. B. Informationen wie die von Ihrem Provider zugewiesene IP-Adresse. Zu den Problematiken, die sich aus dieser systemimmanenten Datenübertragung ergeben können.

Dieses systembedingte Verhalten Ihres Browsers können Sie grundsätzlich nicht verhindern und es bleibt Ihnen deshalb auch nichts anderes übrig, als darauf zu vertrauen, dass ich mit den übermittelten Informationen sensibel umgehe. Weil mir – auch wenn es ein wenig abgedroschen klingen mag – der Schutz Ihrer Daten tatsächlich am Herzen liegt, werde ich die übermittelten Informationen bestmöglich Ihre Interessen vertretend handhaben. Aus diesem Grund erhebe, verarbeite oder nutze ich die übermittelten Informationen nicht zu Analyse-, Werbezwecken oder Ähnlichem. Vielmehr verwende ich diese Informationen – entsprechend der Gesetzesintention – nur zum Zweck der Aufrechterhaltung meiner Webservices. Wenn dieser Zweck erreicht ist, werden diese Informationen umgehend automatisiert aus dem sogenannten Logfile gelöscht. Zur Diensterbringung werden folgende Informationen vom Webserver verarbeitet:

- Die Domain, die aufgerufen wird: **angestoepselt.de**
- Die IP-Adresse des anfragenden Rechners: **192.168.0.10**
- Datum und Uhrzeit des Zugriffs: **Tag/Monat/Jahr:Uhrzeit**
- Um welche Art von HTTP-Request es sich handelt: **GET**
- Die URL, die aufgerufen wird: **/datenschutzhinweis/**
- Webseite, von der aus der Zugriff erfolgt (Referrer-URL): **https://www.angestoepselt.de/**
- Den HTTP-Statuscode: **200**
- Verwendeter Browser und ggf. das Betriebssystem Ihres Rechners (User-Agent):**Mozilla/5.0 (Windows NT 6.1; Win64; x64) Gecko/X Firefox/X**

Die genannten Daten werden durch mich zu folgenden Zwecken verarbeitet:

- Gewährleistung eines reibungslosen Verbindungsaufbaus der Webseite
- Auswertung der Systemsicherheit und -stabilität
- Zu weiteren administrativen Zwecken, wie bspw. der Identifikation von verwaisten Links

Die Rechtsgrundlage für die Datenverarbeitung ist Art.6 Abs.1 S.1 lit.f DSGVO. Mein berechtigtes Interesse folgt aus oben aufgelisteten Zwecken zur Datenerhebung. In keinem Fall verwende ich die erhobenen Daten zu dem Zweck, Rückschlüsse auf Ihre Person zu ziehen.
Hinweis
Jeden Morgen (alle 24 Stunden) um 6:25 Uhr wird das Logfile automatisiert rotiert bzw. gelöscht. Alle serverseitig erfassten personenbeziehbaren Daten sind anschließend nicht mehr existent.

### 2.2 Spenden

Wenn Sie uns eine Spende per Überweisung zukommen lassen, sind wir gesetzlich verpflichtet, Namen und Höhe der Überweisung für das Finanzamt vorzuhalten. Die Überweisungsdaten:

- Name
- Verwendungszweck
- Buchungsdatum
- IBAN
- BIC
- Betrag

werden von der Sparkasse Mainfranken automatisch erfasst und sind anschließend ein Bestandteil des Kontoauszugs, den wir für eine gesetzliche Aufbewahrungsfrist von 10 Jahren vorhalten müssen. Nach Ablauf der 10 Jahre werden die Kontoauszüge gelöscht. Ein Widerspruch bzw. ein Recht auf Löschung dieser Daten nach Art.17 DSGVO ist nicht möglich, da dies gegen die gesetzliche Aufbewahrungsfrist verstoßen würde.

### 2.3 Die Nutzung unserer Kontaktadresse

Bei Fragen jeglicher Art bieten wir Ihnen die Möglichkeit, mit uns über die E-Mail-Adresse [info@angestoepselt.de](mailto:info@angestoepselt.de) Kontakt aufzunehmen. Dabei ist die Angabe einer gültigen E-Mail-Adresse erforderlich, damit wir wissen, von wem die Anfrage stammt und um diese zu beantworten zu können. Weitere Angaben können freiwillig getätigt werden.

Die Datenverarbeitung zum Zwecke der Kontaktaufnahme mit uns erfolgt nach Art.6 Abs.1 S.1 lit.a DSGVO auf Grundlage Ihrer freiwillig erteilten Einwilligung.

## 3. Weitergabe von Daten

Eine Übermittlung Ihrer persönlichen Daten an Dritte zu anderen als den im Folgenden aufgeführten Zwecken findet nicht statt.

Wir geben Ihre persönlichen Daten nur an Dritte weiter, wenn:

- Sie Ihre nach Art.6 Abs.1 S.1 lit.a DSGVO ausdrückliche Einwilligung dazu erteilt haben
- Die Weitergabe nach Art.6 Abs.1 S.1 lit.f DSGVO zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen erforderlich ist und kein Grund zur Annahme besteht, dass Sie ein überwiegendes schutzwürdiges Interesse an der Nichtweitergabe Ihrer Daten haben
- Für die Weitergabe nach Art.6 Abs.1 S.1 lit.c DSGVO eine gesetzliche Verpflichtung besteht
- Dies gesetzlich zulässig und nach Art.6 Abs.1 S.1 lit.b DSGVO für die Abwicklung von Vertragsverhältnissen mit Ihnen erforderlich ist

## 4. Betroffenenrechte

Sie haben das Recht:

- Gemäß Art.15 DSGVO Auskunft über Ihre von uns verarbeiteten personenbezogenen Daten zu verlangen. Insbesondere können Sie Auskunft über die Verarbeitungszwecke, die Kategorie der personenbezogenen Daten, die Kategorien von Empfängern, gegenüber denen Ihre Daten offengelegt wurden oder werden, die geplante Speicherdauer, das Bestehen eines Rechts auf Berichtigung, Löschung, Einschränkung der Verarbeitung oder Widerspruch, das Bestehen eines Beschwerderechts, die Herkunft ihrer Daten, sofern diese nicht bei uns erhoben wurden, sowie über das Bestehen einer automatisierten Entscheidungsfindung einschließlich Profiling und ggf. aussagekräftigen Informationen zu deren Einzelheiten verlangen
- Gemäß Art.16 DSGVO unverzüglich die Berichtigung oder Vervollständigung Ihrer bei uns gespeicherten personenbezogenen Daten zu verlangen
- Gemäß Art.17 DSGVO die Löschung Ihrer bei uns gespeicherten personenbezogenen Daten zu verlangen, soweit nicht die Verarbeitung zur Ausübung des Rechts auf freie Meinungsäußerung und Information, zur Erfüllung einer rechtlichen Verpflichtung, aus Gründen des öffentlichen Interesses oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen erforderlich ist
- Gemäß Art.18 DSGVO die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen, soweit die Richtigkeit der Daten von Ihnen bestritten wird, die Verarbeitung unrechtmäßig ist, Sie aber deren Löschung ablehnen, wir die Daten nicht mehr benötigen, Sie jedoch diese zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen benötigen oder Sie gemäß Art.21 DSGVO Widerspruch gegen die Verarbeitung eingelegt haben
- Gemäß Art.20 DSGVO Ihre personenbezogenen Daten, die Sie uns bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten oder die Übermittlung an einen anderen Verantwortlichen zu verlangen
- Gemäß Art.7 Abs.3 DSGVO Ihre einmal erteilte Einwilligung jederzeit gegenüber uns zu widerrufen. Dies hat zur Folge, dass ich die Datenverarbeitung, die auf dieser Einwilligung beruhte, für die Zukunft nicht mehr fortführen darf
- Gemäß Art.77 DSGVO sich bei einer Aufsichtsbehörde zu beschweren. In der Regel können Sie sich hierfür an die Aufsichtsbehörde Ihres üblichen Aufenthaltsortes oder Arbeitsplatzes wenden

## 5. Widerspruchsrecht

Sofern Ihre personenbezogenen Daten auf Grundlage von berechtigten Interessen gemäß Art.6 Abs.1 S.1 lit.f DSGVO verarbeitet werden, haben Sie das Recht, gemäß Art.21 DSGVO Widerspruch gegen die Verarbeitung Ihrer personenbezogenen Daten einzulegen, soweit dafür Gründe vorliegen, die sich aus Ihrer besonderen Situation ergeben oder sich der Widerspruch gegen Direktwerbung richtet. Im letzteren Fall haben Sie ein generelles Widerspruchsrecht, das ohne Angabe einer besonderen Situation von uns umgesetzt wird.

Möchten Sie von Ihrem Widerrufs- oder Widerspruchsrecht Gebrauch machen, genügt eine E-Mail an »info@angestoepselt.de«.

## 6. Datensicherheit / Datenverarbeitung

Die gesamte Datenverarbeitung dieser Webseite erfolgt auf einem Server (Hosting durch netz-haut GmbH) von Angestöpselt e.V. Wir haben entsprechende technische und organisatorische Schutzmaßnahmen ergriffen, um potenziellen Angreifern ein Kompromittieren des Servers erheblich zu erschweren.

Beim Besuch dieser Webseite wird eine verschlüsselte Verbindung über das verbreitete TLS-Verfahren (Transport Layer Security) in Verbindung mit der jeweils höchsten Verschlüsselungsstufe (sog. Cipher-Suite) initiiert, die von Ihrem Browser unterstützt wird. Ob eine einzelne Seite unseres Internetauftrittes verschlüsselt übertragen wird, erkennen Sie an der geschlossenen Darstellung des Schüssel- beziehungsweise Schloss-Symbols in der URL-Zeile Ihres Browsers.

Im Übrigen verwenden wir geeignete technische und organisatorische Sicherheitsmaßnahmen, um Ihre Daten gegen zufällige oder vorsätzliche Manipulationen, teilweisen oder vollständigen Verlust, Zerstörung oder gegen den unbefugten Zugriff Dritter zu schützen. Unsere getroffenen Sicherheitsmaßnahmen werden entsprechend der technologischen Entwicklung fortlaufend verbessert. Zu den Maßnahmen zählen unter anderem:

- Verschlüsselte Erreichbarkeit via HTTPS auf Basis moderner Cipher-Suites (TLS 1.3 / TLS 1.2 mit AEAD, PFS und 384-Bit-ECDSA-Zertifikat)
- Content-Security-Policy (CSP), bspw. zum Schutz gegen Cross-Site-Scripting
- Vollständiger Verzicht auf Cookies und JavaScript
- Keine Einbindung von Ressourcen aus (unsicheren) Drittquellen
- Moderne Sicherheitsfeatures wie DNSSEC, OCSP-Stapling und CAA
- Einsatz aktueller HTTP-Security-Header wie HSTS, Expect-CT und Referrer-Policy

## 7. Aktualität und Änderung dieser Datenschutzerklärung

Diese Datenschutzerklärung ist aktuell gültig und hat den Stand April 2022.

Durch die Weiterentwicklung unserer Webseite und Angebote darüber oder aufgrund geänderter gesetzlicher beziehungsweise behördlicher Vorgaben kann es notwendig werden, diese Datenschutzerklärung zu ändern. Die jeweils aktuelle Datenschutzerklärung kann jederzeit auf der Webseite unter dem Link von Ihnen abgerufen und ausgedruckt werden.

## 8. Adressverarbeitung

Alle die auf dieser Webseite angegebenen Kontaktinformationen von uns inklusive etwaiger Fotos dienen ausdrücklich nur zu Informationszwecken bzw. zur Kontaktaufnahme. Sie dürfen insbesondere nicht für die Zusendung von Werbung, Spam und ähnliches genutzt werden. Einer werblichen Nutzung dieser Daten wird deshalb hiermit widersprochen. Sollten diese Informationen dennoch zu den vorstehend genannten Zwecken genutzt werden, behalte wir uns etwaige rechtliche Schritte vor.
