---
layout: layouts/page.njk
useForms: true
callout: "Diese Seite befindet sich im Aufbau; das Formular ist noch nicht funktionstüchtig."
---

<style>
#mode-o:not(:checked) ~ #mode-f:not(:checked) ~ [data-payment],
#mode-o:not(:checked) ~ #mode-f:not(:checked) ~ * [data-payment] {
  display: none;
}

#mode-f:not(:checked) ~ [data-payment=custom],
#mode-f:not(:checked) ~ * [data-payment=custom] {
  display: none;
}
</style>

# Vereinsmitglied werden

Schön, dich bei uns begrüßen zu dürfen!
Du kannst direkt hier einen Mitgliedsantrag stellen.
Alternativ kannst du auch klassisch einen [schriftlichen Antrag](/assets/Antrag_Mitgliedschaft.pdf) ausfüllen und uns zukommen lassen.

<form method="post" action="/spenden">

Bitte gib uns ein paar Infos zu dir:

<label class="form-input">
  <span>Name:</span>
  <input type="text" name="contactname" required placeholder="Vorname Nachname" />
</label>

<label class="form-input">
  <span>Email:</span>
  <input type="email" name="contactemail" required placeholder="mail@beispiel.de" />
</label>

<label class="form-input">
  <span>Email:</span>
  <input type="tel" name="contactphone" required placeholder="+49 931 123456" />
</label>

<label class="form-input">
  <span>Adresse:</span>
  <input type="text" name="address" required placeholder="Straße Nr." />
</label>

<label class="form-input">
  <span>Postleitzahl:</span>
  <input type="text" name="zipcode" required placeholder="12345" />
</label>

<label class="form-input">
  <span>Wohnort:</span>
  <input type="text" name="city" required placeholder="Würzburg" />
</label>

Ab wann möchtest du im Verein aufgenommen werden?
Wenn du dieses Feld leer lässt, nehmen wir dich zum nächstmöglichen Zeitpunkt auf.

<label class="form-input">
  <span>Aufnahmedatum:</span>
  <input type="date" name="registrationdate" />
</label>

Nach unserer [Satzung](/ueber-uns/satzung) gibt es bei uns zwei Arten von Mitgliedern - faktisch sind es aber drei Arten:

- _Ordentliche Mitglieder_ engagieren sich im Verein und zahlen in der Regel einen Mitgliedsbeitrag (aktuell 60&nbsp;€ pro Jahr).
  Wie wollen niemanden aufgrund seiner finanziellen Situation ausschließen. Deshalb kannst du auch _freies Mitglied_ werden, ganz ohne jährliche Kosten.
  Ob du den Mitgliedsbeitrag zahlst oder nicht bleibt dir überlassen und wird vertraulich behandelt.
- _Fördermitglieder_ nehmen nicht aktiv an den Vereinsaktivitäten teil und zahlen einen selbst gewählten regelmäßigen Beitrag zur Unterstützung des Vereins. Dieser muss mindestens dem ordentlicher Mitglieder entsprechen.

Wie möchtest du dem Verein beitreten?

<input type="radio" id="mode-o" class="radio-input" name="mode" required value="Ordentlich" />
<div class="form-input">
	<label for="mode-o">Ich möchte als ordentliches Mitglied beitreten.</label>
</div>
<input type="radio" id="mode-b" class="radio-input" name="mode" required value="Befreit" />
<div class="form-input">
	<label for="mode-b">Ich möchte als freies Mitglied beitreten.</label>
</div>
<input type="radio" id="mode-f" class="radio-input" name="mode" required value="Fördermitglied" />
<div class="form-input">
	<label for="mode-f">Ich möchte als Fördermitglied beitreten.</label>
</div>

Bitte
<span role="none" data-payment="custom">wähle einen jährlichen Beitrag für deine Fördermitgliedschaft und</span>
fülle das [Lastschriftmandat](/assets/Lastschriftmandat.pdf) aus (entweder ausgedruckt oder digital unterschrieben):
{data-payment}

<label class="form-input" data-payment="custom">
  <span>Jährlicher Förderbeitrag:</span>
  <input type="number" name="supportamount" min="60" placeholder="&gt; 60€" />
</label>
<label class="form-input" data-payment>
  <span>SEPA-Lastschriftmandat:</span>
  <input type="file" name="document" />
</label>

Noch kurz ein paar Hinweise zum Datenschutz:

- Wir sammeln und speichern personenbezogene Daten von dir und anderen Mitgliedern, um das Vereinsleben aufrechtzuerhalten. Dabei geht es hauptsächlich um das, was du uns in diesem Formular mitgeteilt hast, aber auch z.B. um Daten zum Einloggen in unsere Netzwerksysteme.
- Wir verkaufen diese Daten nicht.
- Der Verein veröffentlicht hin und wieder in Zeitungen, Online-Portalen (z.B. Mastodon) oder anderen Medien Bilder und / oder Namen von Mitgliederinnen oder Mitgliedern. Dabei geht es stets um Vereinsangelegenheiten (wie in unserer [Satzung](/ueber-uns/satzung) beschrieben). Durch deine Mitgliedschaft stimmst du dem zu.
- Im Rahmen geltender Gesetze hast du jederzeit das Recht darauf, deine von uns gespeicherten Daten anzusehen, zu berichtigen oder löschen zu lassen.

Die komplette Datenschutzerklärung für Mitglieder findest du [hier](/assets/Datenschutzerklärung_Mitgliedschaft.pdf).

<label class="form-checkbox">
  <input type="checkbox" name="hints" required>
  <div></div>
  <span>Ich habe die obigen Hinweise gelesen und bin mit der beschriebenen Speicherung und Verarbeitung meiner Daten einverstanden.</span>
</label>

Das war's!
Wir werden uns, nachdem du den Antrag abgeschickt hast, bei dir melden.

<!-- FORM -->

<div class="form-submit">
  <input type="submit" value="Abschicken" />
</div>

</form>
