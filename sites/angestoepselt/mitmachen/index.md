---
layout: layouts/page.njk
useForms: true
---

{% banner
  "",
  "work-banner.jpg",
  "Nahaufnahme eines Laptops mit geöffnetem Gehäuse. Sichtbar ist die ausgebaute Festplatte."
%}
{% endbanner %}

# Bei uns mitmachen

Du bist ein Nerd? Eine Nerdin? Du magst andere Nerds? Dann bist du bei uns
genau richtig!

Wir suchen immer wieder Menschen, die Lust haben, mit ihren eigenen Ideen,
Projekten und ein wenig guter Laune etwas zu unserem Verein beizutragen.
Vielleicht fühlst du dich ja bei irgendwelchen unserer Themen zu Hause:

- Computerteile aus-, um- und einbauen
- Neuaufsetzen von Rechnern
- Server administrieren, Netzwerke einrichten
- Tickets verwalten, Workflows erstellen
- (Remote-)Support bei Fragen und Problemen
- Workshops für Kinder und Jugendliche halten
- Öffentlichkeitsarbeit für den Verein
- Treffen mit Kunden und Spendern

Natürlich sind wir auch offen für eigene Ideen und vollkommen neue Projekte, die zu uns passen.
Sie sollten Menschen helfen, Digitalkompetenz zu erwerben und für mehr Chancengleichheit in
der digitalen Welt sorgen.

Bitte beachte, dass wir leider keine Praktika anbieten. Du kannst aber gerne bei uns ehrenamtlich mitarbeiten, um Kenntnisse und Fertigkeiten in den oben genannten Bereichen zu erlangen.

## Melde dich!

Du kannst gerne dieses Formular verwenden, um mit uns Kontakt aufzunehmen. Wir melden uns dann bei dir, um einen Termin für einen Kennenlerntag (normalerweise Mittwochs zwischen 18 und 20 Uhr) zu vereinbaren. Intern läuft unsere Kommunikation meist über
[Signal](https://signal.org/de/){target="_blank" rel="noopener noreferrer"} – wir schicken dir hier gerne eine
Gruppeneinladung.

<form method="post" action="/mitmachen">

<label class="form-input">
  <span>Name:</span>
  <input type="text" name="contactname" required placeholder="Vorname Nachname" />
</label>

<label class="form-input">
  <span>Email:</span>
  <input type="email" name="contactemail" required placeholder="mail@beispiel.de" />
</label>

<label class="form-input">
  <span>Nachricht:</span>
  <textarea name="message"></textarea>
</label>

<!-- FORM -->

<div class="form-submit">
  <input type="submit" value="Abschicken" />
</div>

</form>

## Mitglied werden

Wenn du schon so begeistert bist, kannst du auch gerne direkt hier ein Mitglied im Verein werden:

- [Zum _Mitgliedsantrag_](/mitmachen/beitreten)

{.form-choices}

Bitte fülle den Antrag aus, unterschreibe ihn und stelle ihn dem Verein zu – entweder per Post oder persönlich.
Wir melden uns dann bei dir.
Natürlich kann auch jeder während der Öffnungszeiten vorbeikommen und persönlich ins Gespräch kommen.
