---
layout: layouts/page.njk
useForms: true
---

# Hardware als Organisation spenden

<form method="post" action="/hardware-spenden/organisation" enctype="multipart/form-data">

Vielen Dank, dass du uns deine gebrauchte Hardware überlassen möchtest! Informationen zu den häufigsten Fragen findest du weiter unten [auf der Seite](#l%C3%B6schung-der-datentr%C3%A4ger). Um die Übergabe möglichst reibungslos zu gestalten, benötigen wir zunächst ein paar
Angaben zu deiner Organisation und wie wir dich erreichen können.

<label class="form-input">
  <span>Name:</span>
  <input type="text" name="organization" required placeholder="Name der Organisation" />
</label>

<label class="form-input">
  <span>Ansprechpartner:in:</span>
  <input type="text" name="contactname" required placeholder="Vorname Nachname" />
</label>

<label class="form-input">
  <span>Email:</span>
  <input type="email" name="contactemail" required placeholder="mail@beispiel.de" />
</label>

Bitte beschreibe uns im Folgenden die Hardwarespende etwas genauer. Wir werden
das Angebot prüfen und alles Weitere direkt besprechen.
Wir würden uns freuen, wenn du eine Inventarliste oder Übersicht mitschickst. Sollte das nicht möglich sein, ist das aber auch
kein Problem.

<label class="form-input">
  <span>Inventarliste:</span>
  <input type="file" name="inventory" />
</label>

<label class="form-input">
  <span>Nachricht:</span>
  <textarea name="message"></textarea>
</label>

<!-- FORM -->

<div class="form-submit">
  <input type="submit" value="Abschicken" />
</div>

</form>

## Löschung der Datenträger

Wir löschen gespendete Festplatten den Vorgaben des Bundesamtes für Sicherheit in der Informationstechnik (BSI) entsprechend (s.u.).

_HDD_-Festplatten löschen wir mit [DBAN](https://dban.org/) oder wahlweise auch zertifiziert mit BCWipe. Dabei werden die Daten mehrfach mit vorgegebenen Zeichen oder Zufallszahlen überschrieben, was ein Wiederherstellen effektiver verhindert als das "einfache" Löschen.

_SSD_-Festplatten werden in einem standardisierten Vorgehen über den Terminal gelöscht.

Das Bundesamt für Sicherheit in der Informationstechnik (BSI) hat eine Zusammenfassung auf seiner [Homepage](https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/Informationen-und-Empfehlungen/Cyber-Sicherheitsempfehlungen/Daten-sichern-verschluesseln-und-loeschen/Daten-endgueltig-loeschen/daten-endgueltig-loeschen_node.html) veröffentlicht.

Es gibt vier gängige Lösungen, die Datenschutzanforderungen deines Unternehmens mit einer Spende an Angestöpselt in Einklang zu bringen:

- Löschung im Unternehmen (gut für uns): Ihr übernehmt die Löschung bzw. das Überschreiben der Daten. Das Installieren von Windows ist nicht notwendig. Im Verein installieren wir dann Linux Mint.
- Löschung im Verein (gängigste Variante): Ihr überlasst uns Computer inklusive Festplatten auf Vertrauensbasis. Wir überschreiben alle Inhalte nach obiger Methode und installieren Linux Mint. Defekte HDD/SSDs vernichten wir mechanisch vor dem Recycling.
- Ihr braucht ein Zertifikat, dass die Festplatte gelöscht ist? Kein Problem, dank der [Spende](https://www.jetico.com/about-us/success-stories/securely-erase-hard-drives-and-centralize-wiping-tasks) von BCWipe ist das nun möglich.
- Spende ohne Datenträger: Ihr baut die Festplatten aus und spendet "nur" die Geräte. Wir rüsten die Computer dann im Verein wieder entsprechend auf. Natürlich freuen wir uns, wenn ihr uns ergänzend bei der Anschaffung neuer SSD Festplatten finanziell unterstützt.

## Weitere Informationen

<dl>
<dt>Spendenquittungen</dt>
<dd>

Für gespendete Hardware stellen wir Sachspendenquittungen nach dem Buchwertprinzip aus. In der Regel beträgt der Buchwert 1,00 € pro Gerät. Bei Neuware oder sehr neuer Hardware teile uns bitte vor der Spende den laufenden Buchwert mit.

</dd>
<dt>Übergabe der Hardware</dt>
<dd>

Wir sind darauf angewiesen, dass ihr die Spenden bei uns im Verein vorbeibringt. Das geht persönlich zu unseren [Öffnungszeiten](/kontakt/#so-kommst-du-zu-uns).
In Ausnahmefällen können wir auch eine Abholung organisieren, was jedoch mit hohem logistischen (wir sind alle berufstätig) und finanziellen (wir müssen einen Transporter mieten) Aufwand verbunden ist.
Bitte habt Verständnis dafür, dass wir deshalb in der Regel auf einen Anlieferung angewiesen sind.

</dd>
<dt>Versand von Hardware</dt>
<dd>

Es ist auch möglich, die Hardware an unseren Verein zu schicken.
Falls ihr eine Spedition beauftragen wollt, teilt uns das bitte unbedingt vorher mit, damit jemand die Lieferung in Empfang nehmen kann.

</dd>
</dl>
