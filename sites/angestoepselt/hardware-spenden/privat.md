---
layout: layouts/page.njk
useForms: true
---

# Private Hardware spenden

Danke, dass du uns deine gebrauchte Hardware überlassen möchtest! Hier kannst du
direkt zum entsprechenden Bereich springen:

- [Zum Bereich _Laptops_](#laptops)
- [Zum Bereich _Desktop-PCs_](#desktop-pcs)
- [Zum Bereich _Drucker_](#drucker)

{.form-choices}

## Laptops

<form method="post" action="/hardware-spenden/privat/laptop">

Um die Übergabe möglichst reibungslos zu gestalten, benötigen wir zunächst ein
paar Kontaktdaten, damit wir auf dich zurückkommen können.

<label class="form-input">
  <span>Name:</span>
  <input type="text" name="contactname" required placeholder="Vorname Nachname" />
</label>

<label class="form-input">
  <span>Email:</span>
  <input type="email" name="contactemail" required placeholder="mail@beispiel.de" />
</label>

Gegebenenfalls kannst du uns im folgenden Feld etwas mehr über das Gerät
erzählen (zum Beispiel die Modellnummer, falls du sie parat hast). Falls nicht,
ist das auch kein Problem.

<label class="form-input">
  <span>Infos zum Gerät:</span>
  <input type="text" name="device" />
</label>

<label class="form-input">
  <span>Nachricht:</span>
  <textarea name="message"></textarea>
</label>

<!-- FORM -->

<div class="form-submit">
  <input type="submit" value="Abschicken" />
</div>

</form>

## Desktop-PCs

Leider können wir aus Platzgründen gegenwärtig keine PC-Spenden entgegennehmen. Laptops nehmen wir weiterhin gerne an, falls sie den [Vorgaben](/hardware-spenden) entsprechen.

## Drucker

Gespendete Drucker müssen voll funktionsfähig und _Laserdrucker_ sein. Weiterhin bitten wir darum,
dass die Drucker inklusive Toner übergeben werden.

Tintenstrahl- und Nadeldrucker sowie dazugehörige Patronen können wir _nicht_ mehr annehmen.
