---
layout: layouts/page.njk
useForms: true
---

{% banner
  "",
  "cpu_close_up.png",
  "Nahaufnahme eines CPUs von unten ."
%} {% endbanner %}

# Hardware spenden

Wir sind allen Spender:innen sehr dankbar, die uns gebrauchte Hardware überlassen,
damit wir sie weitergeben können – nur dadurch ist unser Projekt
überhaupt erst möglich geworden! Wenn du Hardware für einen guten Zweck spenden möchtest, sind wir der ideale Partner dafür. Auf diesen
Seiten kannst du mit uns in Kontakt treten, um eine Übergabe zu organisieren.

- [Als _Privatperson_ spenden](/hardware-spenden/privat)
- [Als _Firma, Verein oder andere Organisation_ spenden](/hardware-spenden/organisation)

{.form-choices}

## Informationen und häufig gestellte Fragen

### Welche Mindestanforderungen muss ein zu spendender Computer erfüllen?

Wir möchten vermeiden, dass unser Verein als "Wertstoffhof" genutzt wird, um
alte Hardware loszuwerden, die nach heutigen Standards nicht mehr verwendbar
ist. Deshalb können wir Geräte, die unter folgenden Anforderungen liegen, leider
nicht annehmen:

<dl>
<dt>Computer</dt>
<dd>

_Für alle_: das Gerät sollte **höchstens 8 Jahre alt** und mindestens
für Windows 7 ausgelegt sein.

_Für Fachleute_: das Gerät sollte mindestens folgenden Spezifikationen erfüllen:

- Prozessor: mindestens Intel-Core-i-Serie / AMD AM3
- Hauptspeicher: mindestens DDR3
- Festplatte: mindestens SATA 250 GB
- Grafikprozessor (sofern vorhanden): ab PCI-E

</dd>
<dt>Festplatten</dt>
<dd>

Wir nehmen SSD-Festplatten in allen Größen an. HDD-Festplatten nehmen wir ab einer Größe von 1 Terabyte an. Alle gespendeten Festplatten werden von uns gelöscht, siehe Informationen weiter unten. 

</dd>
<dt>Bildschirme</dt>
<dd>

Wir nehmen nur Flachbildschirme (TFT) mit mindestens 21,5" (Bildschirmdiagonale 54,6&nbsp;cm) und Bildverhältnis 16:9 an.

</dd>
<dt>Drucker</dt>
<dd>

Gespendete Drucker müssen voll funktionsfähig und _Laserdrucker_ sein. Weiterhin bitten wir darum,
dass die Drucker inklusive Toner übergeben werden.

Tintenstrahl- und Nadeldrucker sowie dazugehörige Patronen können wir _nicht_ mehr annehmen.

</dd>
</dl>

### Werden Spendenquittungen ausgestellt?

_Privatpersonen_: Für gespendete Hardware stellen wir keine Sachspendenquittung aus. Die zuverlässige Ermittlung des Restwertes der Geräte ist uns nicht möglich.

_Unternehmen_: Für gespendete Hardware stellen wir Sachspendenquittungen nach dem Buchwertprinzip aus. In der Regel beträgt der Buchwert 1,00 € pro Gerät. Bei Neuware oder sehr neuer Hardware teile uns bitte vor der Spende den laufenden Buchwert mit.

### Wie wird sichergestellt, dass alle meine Daten von dem Gerät gelöscht sind?

Unser Umgang mit den gespendeten Datenträgern ist sehr sensibel.

Alle Festplatten löschen wir mit [Jetico BCWipe Total WipeOut](https://www.jetico.com/data-wiping/wipe-hard-drives-bcwipe-total-wipeout). Standardmäßig werden HDD Festplatten dabei mit Zufallszahlen überschrieben und SSD-Festplatten sicher mit dem [Secure Erase](https://tinyapps.org/docs/wipe_drives_hdparm.html) Verfahren gelöscht.

Damit folgen wir den [Empfehlungen des Bundesamts für Sicherheit in der Informationstechnik (BSI)](https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/Informationen-und-Empfehlungen/Cyber-Sicherheitsempfehlungen/Daten-sichern-verschluesseln-und-loeschen/Daten-endgueltig-loeschen/daten-endgueltig-loeschen_node.html).

Weitere Informationen zu unserer Nutzung von Jetico BCWipe Total WipeOut findet ihr in der [Erfolgsgeschichte von Angestöpselt und Jetico](https://www.jetico.com/serve/angestopselt-ev-and-jetico-success-story-bcwipe-total-wipeout-leads-more-donated-hard-drives).
