---
layout: layouts/page.njk
---

# Computer reparieren

Sofern es sich für uns einrichten lässt, führen wir auf Wunsch auch Reparaturen von Computern oder Notebooks durch. Dies ist selbstverständlich kostenlos.
Hierfür wird ein gütiger ALGII-, HarzIV-, Rentenbescheid, Behinderten- oder
Asylausweis benötigt. Bitte denke daran, vor der Abgabe des Gerätes **alle
wichtigen Daten zu sichern!** Wir übernehmen keine Haftung für entstandene Schäden
oder Datenverluste.

Bitte kontaktiere uns vor der Reparatur über das
[Kontaktformular](/kontakt/problem). **Aufgrund der sehr hohen Nachfrage führen
wir zurzeit allerdings nur in Ausnahmefällen Reparaturanfragen durch**.
