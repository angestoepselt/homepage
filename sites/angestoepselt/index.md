---
layout: layouts/home.njk
contentClass: expand
eleventyNavigation:
  key: Helfen
  order: 51
  url: /#helfen
---

{% banner
"Angestöpselt",
"home-banner.jpg",
"Kinder und Jugendliche verfolgen die Präsentation eines 3D-Druckers."
%}
Wir schaffen Zugang in die digitale Welt
{% endbanner %}

<section class="page-actions">
  <div>
    <h2 id="helfen">Helfen</h2>
    <p>Du kannst uns auf vielen Wegen helfen: Spende deinen alten Computer. Spende Geld, damit wir Teile zum Aufrüsten kaufen können. Komme vorbei und pack mit an. Egal, wofür du dich entscheidest, wir freuen uns auf deinen Beitrag.</p>
  </div>

  <a href="/computer-beantragen">
    <div>
      <h3>Computer bekommen</h3>{% if config.applicationsClosed %}<em class="inline-callout">Aktuell keine Hardware verfügbar.</em>{% endif %}
    </div>
    <svg class="action-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 219 175">
      <linearGradient id="emphasis-gradient-sun" class="emphasis-gradient">
        <stop offset="0%" />
        <stop offset="100%" />
      </linearGradient>
      <circle class="emphasis sun" cx="100" cy="85" r="35" fill="url(#emphasis-gradient-sun)" />
      <g>
        <path d="M188 63h-28a2 2 0 1 0 0 5h28a2 2 0 1 0 0-5"/>
        <path d="M188 76h-28a2 2 0 1 0 0 5h28a2 2 0 1 0 0-5"/>
        <path d="M191 91a2 2 0 0 0-3-2h-28a2 2 0 1 0 0 5h28a2 2 0 0 0 2-3"/>
        <path d="M174 113a9 9 0 1 0 9 10 9 9 0 0 0-9-10m0 14a4 4 0 1 1 4-4 4 4 0 0 1-4 4"/>
        <path d="M174 136a9 9 0 1 0 9 9 9 9 0 0 0-9-9m0 13a4 4 0 1 1 4-4 4 4 0 0 1-4 4"/>
        <path d="M156 102h-4a2 2 0 1 0 0 5h4a2 2 0 1 0 0-5"/>
        <path d="M170 105a2 2 0 0 0-2-3h-4a2 2 0 1 0 0 5h4a2 2 0 0 0 2-2"/>
        <path d="M204 99a2 2 0 0 0 3-2V61a9 9 0 0 0-9-9h-32V41a10 10 0 0 0-10-10H74a2 2 0 0 0 0 5h82a5 5 0 0 1 5 5v11h-6v-7a4 4 0 0 0-4-4H56a4 4 0 0 0-4 4v80a4 4 0 0 0 4 4h8a2 2 0 0 0 0-5h-7V46h93v6a9 9 0 0 0-9 9v63H74a2 2 0 1 0 0 5h67v5H51a5 5 0 0 1-5-5V41a5 5 0 0 1 5-5h13a2 2 0 0 0 0-5H51a10 10 0 0 0-10 10v88a10 10 0 0 0 10 10h20v5a5 5 0 0 0 5 5h9v10h-9a2 2 0 1 0 0 5h55a2 2 0 0 0 0-5h-9v-10h9a5 5 0 0 0 5-5v-5h5v16a9 9 0 0 0 9 9h48a9 9 0 0 0 9-9v-48a2 2 0 0 0-5 0v48a4 4 0 0 1-4 4h-48a4 4 0 0 1-4-4V61a4 4 0 0 1 4-4h48a4 4 0 0 1 4 4v36a2 2 0 0 0 2 2m-87 60H90v-10h27Zm14-15H76v-5h55Z"/>
      </g>
    </svg>
  </a>

  <a href="/hardware-spenden">
    <h3>Computer spenden</h3>
    <svg class="action-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 219 175">
      <linearGradient id="emphasis-gradient-heart-left" class="emphasis-gradient">
        <stop offset="0%" />
        <stop offset="100%" />
      </linearGradient>
      <path
        class="emphasis heart-left"
        d="M70 14a21 21 0 0 0-23 2 21 21 0 0 0-24-2 20 20 0 0 0-11 17 18 18 0 0 0 1 6c4 14 24 32 34 32s30-18 33-32a19 19 0 0 0 1-6 20 20 0 0 0-11-17"
        fill="url(#emphasis-gradient-heart-left)"
      />
      <g>
        <path d="M188 63h-28a2 2 0 1 0 0 5h28a2 2 0 1 0 0-5"/>
        <path d="M188 76h-28a2 2 0 1 0 0 5h28a2 2 0 1 0 0-5"/>
        <path d="M191 91a2 2 0 0 0-3-2h-28a2 2 0 1 0 0 5h28a2 2 0 0 0 2-3"/>
        <path d="M174 113a9 9 0 1 0 9 10 9 9 0 0 0-9-10m0 14a4 4 0 1 1 4-4 4 4 0 0 1-4 4"/>
        <path d="M174 136a9 9 0 1 0 9 9 9 9 0 0 0-9-9m0 13a4 4 0 1 1 4-4 4 4 0 0 1-4 4"/>
        <path d="M156 102h-4a2 2 0 1 0 0 5h4a2 2 0 1 0 0-5"/>
        <path d="M170 105a2 2 0 0 0-2-3h-4a2 2 0 1 0 0 5h4a2 2 0 0 0 2-2"/>
        <path d="M204 99a2 2 0 0 0 3-2V61a9 9 0 0 0-9-9h-32V41a10 10 0 0 0-10-10H74a2 2 0 0 0 0 5h82a5 5 0 0 1 5 5v11h-6v-7a4 4 0 0 0-4-4H56a4 4 0 0 0-4 4v80a4 4 0 0 0 4 4h8a2 2 0 0 0 0-5h-7V46h93v6a9 9 0 0 0-9 9v63H74a2 2 0 1 0 0 5h67v5H51a5 5 0 0 1-5-5V41a5 5 0 0 1 5-5h13a2 2 0 0 0 0-5H51a10 10 0 0 0-10 10v88a10 10 0 0 0 10 10h20v5a5 5 0 0 0 5 5h9v10h-9a2 2 0 1 0 0 5h55a2 2 0 0 0 0-5h-9v-10h9a5 5 0 0 0 5-5v-5h5v16a9 9 0 0 0 9 9h48a9 9 0 0 0 9-9v-48a2 2 0 0 0-5 0v48a4 4 0 0 1-4 4h-48a4 4 0 0 1-4-4V61a4 4 0 0 1 4-4h48a4 4 0 0 1 4 4v36a2 2 0 0 0 2 2m-87 60H90v-10h27Zm14-15H76v-5h55Z"/>
      </g>
    </svg>
  </a>

  <a href="/mitmachen">
    <h3>Zeit spenden</h3>
    <svg class="action-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 219 175">
      <linearGradient id="emphasis-gradient-heart-right" class="emphasis-gradient">
        <stop offset="0%" />
        <stop offset="100%" />
      </linearGradient>
      <path
        class="emphasis heart-right"
        d="M178 14a21 21 0 0 0-24 2 21 21 0 0 0-23-2 20 20 0 0 0-11 17 19 19 0 0 0 1 6c3 14 24 32 33 32s31-18 34-32a19 19 0 0 0 1-6 20 20 0 0 0-11-17"
        fill="url(#emphasis-gradient-heart-right)"
      />
      <g>
          <path d="M100 41a3 3 0 0 0 3-3v-2a3 3 0 1 0-6 0v2a3 3 0 0 0 3 3"/>
          <path d="M100 135a3 3 0 0 0-3 3v2a3 3 0 0 0 6 0v-2a3 3 0 0 0-3-3"/>
          <path d="M50 85h-2a3 3 0 0 0 0 6h2a3 3 0 1 0 0-6"/>
          <path d="M152 85h-2a3 3 0 1 0 0 6h2a3 3 0 1 0 0-6"/>
          <path d="M66 49a3 3 0 1 0-5 4l2 1a3 3 0 0 0 4 1v-1a3 3 0 0 0 0-4Z"/>
          <path d="m63 121-2 1a3 3 0 0 0 0 4v1a3 3 0 0 0 5 0l1-1a3 3 0 0 0-4-5"/>
          <path d="m135 49-1 1a3 3 0 0 0-1 4l1 1a3 3 0 0 0 4 0l1-2a3 3 0 0 0-4-4"/>
          <path d="M103 87V59a3 3 0 1 0-6 0v29a3 3 0 0 0 1 2l33 33a3 3 0 0 0 5 0 3 3 0 0 0 0-4Z"/>
          <path d="M100 18a70 70 0 1 0 70 70 70 70 0 0 0-70-70m0 134a64 64 0 1 1 64-64 64 64 0 0 1-64 64"/>
      </g>
    </svg>
  </a>

  <a href="/spenden">
    <h3>Geld spenden</h3>
    <svg class="action-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 219 175">
      <linearGradient id="emphasis-gradient-coin" class="emphasis-gradient">
        <stop offset="0%" />
        <stop offset="100%" />
      </linearGradient>
      <path d="m108 63 26 1a84 84 0 0 0-26-1m45-6-2-1h-1l-56-2-4 2c-7-6-17-7-21-7a5 5 0 0 0-5 5 39 39 0 0 0 0 7 43 43 0 0 0-6 1 5 5 0 0 0-5 5 31 31 0 0 0 4 14l-1 1-5 5a21 21 0 0 1-11 5 8 8 0 0 0-7 9l4 21 5 5h1a33 33 0 0 0 7 4 38 38 0 0 1 5 2 27 27 0 0 1 9 8l1 1a33 33 0 0 0 17 10v4a8 8 0 0 0 7 8h18a8 8 0 0 0 8-8h15a8 8 0 0 0 8 8h18a8 8 0 0 0 7-8v-10a48 48 0 0 0 23-38c1-21-12-41-33-51m-84-3a29 29 0 0 1 16 4l-8 5a25 25 0 0 0-8-2 32 32 0 0 1 0-7M59 67a38 38 0 0 1 7-1h1a16 16 0 0 1 12 5 8 8 0 0 1 2 8 12 12 0 0 1-10 6c-5 0-12-6-12-18Zm122 41a43 43 0 0 1-18 32 3 3 0 0 0-2-1 3 3 0 0 0-3 2v15a3 3 0 0 1-2 3h-18a3 3 0 0 1-3-3v-7a3 3 0 0 0-5 0v2a93 93 0 0 1-15 0v-2a3 3 0 0 0-5 0v7a3 3 0 0 1-3 3H89a3 3 0 0 1-2-3v-12a3 3 0 0 0-5 0v2a26 26 0 0 1-13-7l-1-1a32 32 0 0 0-11-10 44 44 0 0 0-5-2 29 29 0 0 1-6-3h-1a17 17 0 0 1-3-2l-4-21a3 3 0 0 1 3-3c4-1 10-2 14-6l5-6a15 15 0 0 0 11 5 17 17 0 0 0 15-8 13 13 0 0 0-2-14 16 16 0 0 0-2-2l5-3a64 64 0 0 1 9-5h1a31 31 0 0 0 4 6h-1l-4 1a3 3 0 0 0 1 5 78 78 0 0 1 9-2c7-1 19-1 30 2a41 41 0 0 1 13 7 3 3 0 0 0 3-4 45 45 0 0 0-10-6l-2-1a30 30 0 0 0 7-6h1l3 2c19 9 31 27 30 46" />
      <path
        class="emphasis coin"
        d="M154 42a31 31 0 0 1-3 14 30 30 0 0 1-3 5 32 32 0 0 1-12 10c-11-4-23-3-30-3a31 31 0 1 1 48-26"
        fill="url(#emphasis-gradient-coin)"
      />
    </svg>
  </a>
</section>

{% section %}

## Projekte

{% tabs %}
{% tab "Tinkerfestival" %}

### Tinkerfestival

Wir sind davon überzeugt, dass großartige, innovative Ideen nicht nur in Forschungslaboren entstehen, sondern auf spielerisch inspirierende Weise in den Köpfen aller Menschen. Tinkerfestivals bieten die Möglichkeit, Ideen auszutauschen, Prototypen zu entwickeln und an technischen Lösungen zu tüfteln.
Jedes Festival steht unter einem Motto, zum Beispiel 'Die Stadt der Zukunft'.

Mitmachen kann jeder, der sich mit den Themen auseinandersetzen möchte.

{% endtab %}
{% tab "CoderDojo" %}

### CoderDojo

Technik verstehen und selber beherrschen. Im Kern sollen die [CoderDojos](https://www.coderdojo-wue.de/){target="_blank" rel="noopener noreferrer"} Jugendlichen genau das ermöglichen. Programmieren, 3D-Druck, Videoschnitt, Kryptografie sind nur einige der Themen. Unabhängig deiner Kentnisse ist für jeden etwas dabei.

Entdecken, basteln, ausprobieren - kostenlos und offen für alle jungen Entdecker:innen.

{% endtab %}
{% tab "Computerspende" %}

### Computerspende

Jeder Mensch, der einen Computer braucht, soll einen Computer bekommen. Damit alle Menschen die gleichen Chancen haben und in der Gemeinschaft mitmachen können, verschenken wir Computer. Wir sammeln und reparieren Computer, die von Firmen und Privatpersonen gespendet werden. Wir verwenden dafür freie und kostenlose Computerprogramme.

Alle Menschen, die sich keinen Computer kaufen können, bekommen bei uns einen [Computer](/computer-beantragen).

{% endtab %}
{% endtabs %}
{% endsection %}

{% section true %}

## Über uns

Angestöpselt steht für **Teilhabe für alle, gleiche Chancen und eine offene Gesellschaft**. Darum verwenden wir leichte Sprache. Wir nutzen freie Computerprogramme. Und unser Angebot steht allen Menschen offen.

Wir finden: Jeder Mensch, der einen Computer braucht, soll einen Computer bekommen. Manche Menschen haben nicht genug Geld, um sich einen Computer zu kaufen. Das grenzt sie aus. Damit alle Menschen an der Gesellschaft mitmachen können, verschenken wir Computer.

[Weiterlesen](/ueber-uns){.cta-link}

{% endsection %}

{% section %}

## Würzburg, Deutschland und die Welt

Wir finden: Jeder Mensch, der einen Computer braucht, soll einen Computer bekommen. Allerdings sind wir nur wenige, die mithelfen. Deswegen können wir nur den Menschen in Würzburg und Umgebung damit helfen. Ein Versand der Computer ist **nicht** möglich. Es gibt aber in ganz Deutschland Organisationen, die die gleiche Idee verfolgen.

[Weiterlesen](/ueber-uns/andere){.cta-link}

{% endsection %}
