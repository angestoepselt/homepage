---
layout: layouts/page.njk
useForms: true
---

# Arbeitsgrundlagen bei Angestöpselt

> Hey! Diese Seite ist primär für den internen Gebrauch bei uns im Verein gedacht. Du darfst natürlich trotzdem reinschnuppern – falls wir dein Interesse geweckt haben, [mach doch mit](/mitmachen)!

<br />
<br />

Auf dieser Seite wollen wir die wichtigsten Dinge zum Vereinsleben zusammenfassen.
Hier kannst du alles nachlesen, was wichtig sein könnte.
Falls dir etwas fehlt, melde dich bitte beim Homepage-Team oder ergänze es selbst!

## Ansprechpartner und Kommunikation

Zurzeit verwenden wir hauptsächlich diese Kommunikationskanäle:

- [_Signal_-Gruppen (schreib uns, damit wir dich aufnehmen können)](/mitmachen/#melde-dich!)
- [Für technisches: _Codeberg_](https://codeberg.org/angestoepselt)
- [Für Online-Besprechungen: _Senfcall_](https://public.senfcall.de/angestoepselt)

{.form-choices .narrow}

Falls dir für eine der Plattformen die Zugriffsrechte fehlen, [melde dich](/mitmachen/#melde-dich!).
Für bestimmte Angelegenheiten findest du hier eine Liste erster Ansprechpartner:

- Vorstand: Lukas, Matthias, Tobias
- IT-Verwaltung: Matthias
- Homepage: Yannik
- Computerspende: Sebastian, Lukas, Tobias
- CoderDojo: Maximilian, Yannik

## Schreibstil

Wir versuchen im Schriftverkehr grundsätzlich möglichst leichte, barrierearme Sprache zu verwenden.

Für gendergerechte Sprache verwenden wir folgende Regeln (in absteigender Priorität):

- Wenn möglich neutrale Formulierungen verwenden, z.B. Menschen, Personen, Hilfesuchende, Nerds
- Alternativ maskuline und feminine Form ausschreiben, z.B. Teilnehmerinnen und Teilnehmer
- Dritte Möglichkeit: Binnenzeichen ":" um alle geschlechtsidentitäten zu inkludieren, z.B. Antragsteller:innen, Spender:innen

## Linkübersicht

- [
    ![Zammad](/assets/logos/zammad.png) Ticketsystem
  ](https://ticket.z31.it){target="_blank" rel="noopener noreferrer"}
- [
    ![Snipe-IT](/assets/logos/snipeit.png) Computerverwaltung
  ](https://computer.z31.it){target="_blank" rel="noopener noreferrer"}
- [
    ![Bitwarden](/assets/logos/bitwarden.png) Passwortmanager
  ](https://pass.z31.it){target="_blank" rel="noopener noreferrer"}
- [
    ![Codeberg](/assets/logos/codeberg.png) freier Git Server
  ](https://codeberg.org/angestoepselt){target="_blank" rel="noopener noreferrer"}
- [
    ![Portainer](/assets/logos/portainer.png) Verwaltung Infrastruktur
  ](https://portainer.pub.z31.it){target="_blank" rel="noopener noreferrer"}
- [
    ![Drone](/assets/logos/drone.svg) Drone Buildserver
  ](https://drone.z31.it){target="_blank" rel="noopener noreferrer"}
- [
    ![n8n](/assets/logos/n8n.png) n8n Automation
  ](https://n8n.z31.it){target="_blank" rel="noopener noreferrer"}
- [
    ![Auslagenerstattung](/android-chrome-192x192.png) Formular für Erstattung von Auslagen
  ](https://auslagen.z31.it/){target="_blank" rel="noopener noreferrer"}

{.link-grid}