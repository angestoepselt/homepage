---
layout: layouts/page.njk
eleventyNavigation:
  key: Über uns
  order: 100
extraStylesheets: ['timeline']
contentClass: expand
---

# Über uns

## Selbstverständnis

Angestöpselt e.V. steht online und offline für:

- Teilhabe für alle – wir verwenden leichte Sprache.
- Gleiche Chancen – unser Angebot steht allen offen.
- Eine offene und faire Gesellschaft – wir verwenden freie Computerprogramme.

### Was uns antreibt

Wir finden: _Jeder Mensch, der einen Computer braucht, soll einen Computer bekommen._ Manche Menschen haben nicht genug Geld, um sich einen Computer zu kaufen. Das grenzt sie aus. Wir arbeiten daran, dass alle Leute in der Gemeinschaft mitmachen können.

Damit alle Menschen die gleichen Chancen haben, verschenken wir Computer.

### Was wir tun

- Wir sammeln und reparieren Computer. Diese verschenken wir dann.
- Wir helfen Menschen, besser mit Computern und dem Internet umzugehen.
- Wir verwenden kostenlose und freie Computerprogramme.
- Wir organisieren Kurse und Techniktreffen.
- Wir erklären Menschen Technik.

## Andere Projekte

Wir sind nicht der einzige Verein, der solche Projekte verfolgt. In ganz Deutschland sind Projekte mit verwandten Zielen vertreten:

[Unsere Partner](partner){.cta-link}

[Andere Vereine und Projekte](andere){.cta-link}

## Vereinsgeschichte

{% timeline "2011" %}

### Vereinsgründung

Steffen Hock und Christoph Fischer gründen den Verein nach dem Vorbild der
Computerspende Hamburg. Es gibt noch keine eigenen Räume. Deswegen wird im eigenen Keller Hardware gesammelt und repariert. Aus dem Kofferraum heraus werden Computer verteilt.

{% endtimeline %}
{% timeline "2012" %}

### Jobcenter

Umzug in die ersten eigenen Räumlichkeiten in der Frankfurter Straße 74, Zellerau. Durch eine Zusammenarbeit mit dem Jobcenter Würzburg steigt die Nachfrage. ALG-II-Empfänger:innen sind die Hauptabnehmer der gespendeten Geräte. Die ersten 50 Computer sind geschafft. Der Verein ist auf 10 Mitglieder gewachsen.

{% endtimeline %}
{% timeline "2014" %}

### Gute Presse

Die lokale Presse berichtet immer häufiger über Angestöpselt. Seit Vereinsgründung wurden über 300 Computer ausgegeben. 30 Mitglieder kümmern sich um die Anliegen der Kundinnen und Kunden. Es gibt jetzt auch Infoveranstaltungen und Einführungen in Ubuntu.

{% endtimeline %}
{% timeline "2015" %}

### Hochsaison

Als 2015 viele Menschen auch in Würzburg Zuflucht finden, steigt die Nachfrage
nach unserem Projekt enorm an. Wir haben alle Hände voll zu tun, möglichst
vielen Menschen einen eigenen Computer zur Verfügung zu stellen.

{% endtimeline %}
{% timeline "2015" %}

### Deutscher Engagementpreis

Angestöpselt e.V. gewinnt den Deutschen Engagementpreis. Arbeitssuchende, Flüchtlinge, Rentner:innen, Schüler:innen, Studierende - der Kreis der Interessierten wächst stetig. Im Jahr 2016 wird Rechner Nummer 1.000 verschenkt.

{% endtimeline %}
{% timeline "2017" %}

### CoderDojo

Mit dem [CoderDojo](/#coderdojo) haben wir unser zweites großes Projekt im Verein
aufgenommen. Seitdem haben wir etliche kostenlose Veranstaltungen zusammen mit
Ehrenamtlichen angeboten. Dabei können sich Kinder und Jugendliche vielen
Themen bequem und spielerisch nähern, zum Beispiel Programmieren,
Kryptografie oder Videoproduktion.

{% endtimeline %}
{% timeline "2018" %}

### Umzug #2

Aufgrund der zunehmenden Bekanntheit erhalten wir immer häufiger Firmenspenden. Schnell stapeln sich auf den 40&nbsp;m² die Computer bis unter die Decke. Es erfolgt der Umzug in die Zeller Str. 29/31, 97082 Würzburg - dort sind wir auch heute noch zu finden. Anmerkung aus der Zukunft: auch das größere Lager ist natürlich schon wieder bis unter die Decke voll gestopft.

{% endtimeline %}
{% timeline "2019" %}

### Tinkerfestival

Das [Tinkerfestival](/#tinkerfestival) wird von Angestöpselt unterstützt. Als Verein für Digitalkompetenz fördert Angestöpselt die kreative Verknüpfung von Zukunftsthemen mit digitalen Lösungsansätzen. Thema 2019: Die Stadt der Zukunft.

{% endtimeline %}
{% timeline "2020" %}

### Corona

Mit Ausbruch der Pandemie verschiebt sich der Fokus der Computerspende ein weiteres Mal. Alle Schulen sind geschlossen. Viele Schüler:innen brauchen für das Homeschooling einen Computer. Um möglichst vielen Menschen helfen zu können, verschickt unser Verein deutschlandweit Laptops. Innerhalb eines Jahres werden über 400 Haushalte mit einem Computer versorgt.

{% endtimeline %}
{% timeline "…" %}

### Und heute

Die Nachfrage nach Computern ist ungebrochen. Während auf der einen Seite Hardware in immer kürzeren Zeiträumen ausgetauscht wird, fehlt auf der anderen Seite immer mehr Menschen das Geld, sich einen Computer leisten zu können. Die Computerspende von Angestöpselt schafft hier eine Brücke. Für unsere Arbeit suchen wir immer [Helfer:innen](/mitmachen), die uns unterstützen.

{% endtimeline %}
