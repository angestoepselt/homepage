---
layout: layouts/page.njk
tags:
  - legal
eleventyNavigation:
  key: Partner
  order: 10
---

# Unsere Kooperationspartner

Ohne unsere zahlreichen ehrenamtlichen Mitglieder wären viele unserer Projekte unmöglich. Das Gleiche gilt für die vielen Firmen, Institutionen und anderen Vereine, die uns auf verschiedene Weisen unterstützen:

- [
    ![Clario](/assets/logos/clario.jpg) Regelmäßige Computerspenden
  ](https://clario.com){target="_blank" rel="noopener noreferrer"}
- [
    ![iWelt AG](/assets/logos/iwelt.png) Räume für CoderDojo
  ](https://www.iwelt.de){target="_blank" rel="noopener noreferrer"}
- [
    ![Kulturtafel Würzburg](/assets/logos/kulturtafel.jpg) Unterstützt Bedürftige durch kostenlose Event-tickets
  ](https://kulturtafel4.wordpress.com){target="_blank" rel="noopener noreferrer"}
- [
    ![Treffpunkt Ehrenamt - Freiwilligen-Agentur Würzburg](/assets/logos/treffpunkt-ehrenamt.jpg) Würzburger Vereinsnetzwerk
  ](https://www.freiwilligenagentur-wuerzburg.de/freiwilligenagentur/index.html){target="_blank" rel="noopener noreferrer"}
- [
    ![wandelmut Würzburg](/assets/logos/wandelmut.jpg) Würzburger Vereinsnetzwerk
  ](https://wandelmut.org){target="_blank" rel="noopener noreferrer"}
- [
    ![Stadt Würzburg](/assets/logos/stadt.jpg) Regelmäßige Geldspenden
  ](https://www.wuerzburg.de){target="_blank" rel="noopener noreferrer"}
- [
    ![Würzburg Umwelt- und Naturstiftung](/assets/logos/umwelt-naturstiftung.png) Unkostenzuschuss 2019
  ](https://umweltstiftung-wuerzburg.de/){target="_blank" rel="noopener noreferrer"}
- [
    ![Wegmann Automotive](/assets/logos/wegmann.jpg)
  ](https://www.wegmann-automotive.com/de/startseite/){target="_blank" rel="noopener noreferrer"}
- [
    ![netz-haut GmbH](/assets/logos/netzhaut.png) Bereitstellung eines Managed Servers
  ](https://netzhaut.de/){target="_blank" rel="noopener noreferrer"}
- [
    ![Fusic](/assets/logos/fusic.png) Bereitstellung eines Root Servers
  ](https://fusic.de/){target="_blank" rel="noopener noreferrer"}
- [
    ![iFixit - Kaputt muss nicht das Ende sein](/assets/logos/ifixit.jpg) Spende von Werkzeug
  ](https://de.ifixit.com/){target="_blank" rel="noopener noreferrer"}
- [
    ![Flyeralarm](/assets/logos/flyeralarm.png) Regelmäßige Hardwarespenden
  ](https://www.flyeralarm.com/de/){target="_blank" rel="noopener noreferrer"}


{.link-grid}

{# Shuffle the list after page load so that we get a new order every time. #}
<script>
(() => {
  const listElement = document.querySelector('ul.link-grid');
  const listItemHtmls = [];
  listElement.querySelectorAll('li').forEach(element =>
    listItemHtmls.splice(
      Math.floor(Math.random() * (listItemHtmls.length + 1)),
      0,
      element.outerHTML,
    )
  );
  listElement.innerHTML = listItemHtmls.join("");
})();
</script>
