---
layout: layouts/page.njk
useForms: true
---

# Würzburg, Deutschland und die Welt

Wir finden: Jeder Mensch, der einen Computer braucht, soll einen Computer bekommen. Allerdings sind wir nur wenige Menschen, die mithelfen. Deswegen können wir nur den Menschen in Würzburg und Umgebung damit helfen. Ein Versand der Computer ist **nicht** möglich. Es gibt aber in ganz Deutschland Organisationen, die die gleiche Idee verfolgen.

Der Verein Computertruhe e.V. aus Freiburg hat auf seiner Webseite eine sehr gute [Übersicht](https://umap.openstreetmap.fr/de/map/neues-leben-fur-gebrauchte-hardware_728960#6/52.052/9.514){target="_blank" rel="noopener noreferrer"}. Vielleicht findest du dort eine Organisation in deiner Nähe.

[Deutschlandkarte](https://umap.openstreetmap.fr/de/map/neues-leben-fur-gebrauchte-hardware_728960#6/52.052/9.514){.cta-link}{target="_blank" rel="noopener noreferrer"}

Die Initiative ["Hey Alter!"](https://heyalter.com/){target="_blank" rel="noopener noreferrer"} kann dir helfen, wenn du einen Rechner für das Homeschooling benötigst. Bei "Hey Alter!" können Schülerinnen und Schüler in vielen Städten einen gebrauchten Rechner erhalten.

[Homeschooling Computer](https://heyalter.com/){.cta-link}{target="_blank" rel="noopener noreferrer"}

Du brauchst gleich ein ganzes Klassenzimmer voll Computer? Das weltweite Netzwerk [Labdoo](https://labdoo.org){target="_blank" rel="noopener noreferrer"} ermöglicht Kindern digitale Teilhabe im In- und Ausland.

[Weltweites Netzwerk](https://labdoo.org){.cta-link}{target="_blank" rel="noopener noreferrer"}
