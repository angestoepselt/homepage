---
layout: page
tags:
  - legal
eleventyNavigation:
  key: Satzung
  order: 20
---

# Aktuelle Satzung von Angestöpselt

Satzung des Vereins „Angestöpselt e. V. – Verein für Digitalkompetenz“

## §&nbsp;1 Name, Sitz, Vereinsjahr

1. Der Name des Vereins lautet "Angestöpselt – Verein für Digitalkompetenz e. V." (Kurzform "Angestöpselt"). Er ist in das Vereinsregister eingetragen.
2. Der Sitz des Vereins ist Würzburg.
3. Ein Vereinsjahr ist ein Kalenderjahr.

## §&nbsp;2 Vereinszweck

1. Zweck des Vereins ist es die Förderung mildtätiger Zwecke im Sinne des §&nbsp;53 AO.
2. Der Verein richtet seine Tätigkeit darauf, einzelne Personen zu unterstützen,
   1. welche persönlich bedürftig sind, d.h. die infolge ihres körperlichen, geistigen oder seelischen Zustands auf die Hilfe anderer angewiesen sind oder
   2. wirtschaftlich bedürftig sind, d.h. deren Bezüge nicht höher sind als das 4fache des Regelsatzes der Sozialhilfe i.S.d. §&nbsp;22 des Bundessozialhilfegesetzes; beim Alleinstehenden oder Haushaltsvorstand tritt an die Stelle des 4fachen das 5fache des Regelsatzes oder
   3. deren wirtschaftliche Lage aus besonderen Gründen zu einer Notlage geworden ist oder indem der Verein durch die Bereitstellung einer Computeranlage den Zugriff auf das Internet für etwa die Suche eines Arbeitsplatzes, zur Kommunikation und zur Hilfe bei den Hausaufgaben ermöglicht.
   4. Sowie andere mildtätig anerkannte Organisationen, welche dem Wohl der Allgemeinheit dienen. Ebenfalls möglich sind Spenden an ausländische mildtätig anerkannte Organisationen, welche einem sozialen Zweck dienen.
      {type=a}
3. Die mildtätigen Satzungszwecke sollen insbesondere verwirklicht werden durch die Reparatur, Aufarbeitung und Abgabe von gespendeten PC – Gerätschaften und ggf. Software. Des Weiteren soll der Vereinszweck verwirklicht werden durch Hilfestellung und Weiterbildung in Seminaren zur Nutzung der zur Verfügung gestellten PC – Gerätschaften und selbstständigen Problemlösung an diesen.

## §&nbsp;3 Mildtätigkeit und Mittelverwendung

1. Der Verein verfolgt ausschließlich und unmittelbar mildtätige Zwecke im Sinne des Abschnitts „steuerbegünstigte Zwecke“ der Abgabenordnung (§51ff).
2. Der Verein verfolgt keine eigenwirtschaftlichen Zwecke.
3. Mittel des Vereins dürfen nur für satzungsgemäße Aufgaben verwendet werden. Die Mitglieder erhalten keine Zuwendungen aus Mitteln des Vereins.
4. Es darf keine Person durch Ausgaben, die dem Zweck des Vereins fremd sind, oder durch unverhältnismäßig hohe Vergütungen begünstigt werden.

## §&nbsp;4 Vergütung für die Vereinstätigkeit

1. Die Vereins- und Organämter werden grundsätzlich ehrenamtlich ausgeführt.
2. Bei Bedarf können Vereinsämter im Rahmen der haushaltsrechtlichen Möglichkeiten entgeltlich auf der Grundlage eines Dienstvertrages oder gegen Zahlung einer angemessenen Aufwandsentschädigung – auch über den Höchstsätzen nach §&nbsp;3 Nr. a EStG – ausgeübt werden.
3. Entscheidungen über eine entgeltliche Vereinstätigkeit nach Absatz 2. trifft die Mitgliederversammlung. Gleiches gilt für die Vertragsinhalte und die Vertragsbeendigung.
4. Der Vorstand ist ermächtigt, Tätigkeiten für den Verein gegen Zahlung einer angemessenen Vergütung oder Aufwandsentschädigung zu beauftragen. Maßgebend ist die Haushaltslage des Vereins.
5. Zur Erledigung der Geschäftsaufgaben und zur Führung der Geschäftsstelle ist der Vorstand ermächtigt, im Rahmen der haushaltsrechtlichen Möglichkeiten, hauptamtliche Beschäftigte anzustellen.
6. Im Übrigen haben die Mitglieder und Mitarbeiter des Vereins einen Aufwendungsersatzanspruch nach §&nbsp;670 BGB für solche Aufwendungen, die ihnen durch die Tätigkeit für den Verein entstanden sind. Hierzu gehören insbesondere Fahrtkosten, Reisekosten, Porto, Telefon, usw.
7. Der Anspruch auf Anwendungsersatz kann nur innerhalb des laufenden Geschäftsjahres geltend gemacht werden. Erstattungen werden nur gewährt, wenn die Aufwendungen mit Belegen und Aufstellungen, die prüffähig sein müssen, nachgewiesen werden.
8. Vom Vorstand können per Beschluss im Rahmen der steuerrechtlichen Möglichkeiten Grenzen über die Höhe des Aufwendungsersatzes nach §&nbsp;670 BGB festgesetzt werden.
9. Weitere Einzelheiten regelt die Finanzordnung des Vereins, die vom Vorstand erlassen und geändert wird.

## §&nbsp;5 Mitgliedschaft, Mitgliedsbeiträge

1. Die Mitgliedschaft im Verein kann jede voll geschäftsfähige, natürliche Person oder jede juristische Person erwerben, die gewillt ist, den Vereinszweck zu fördern.
2. Die Höhe der Mitgliedsbeiträge wird durch die Beitragsordnung geregelt.
3. Die Beitragsordnung wird von der Mitgliederversammlung beschlossen.
4. Der Aufnahmeantrag ist schriftlich zu stellen. Über die Aufnahme der Mitglieder entscheidet der Vorstand.
5. Es gibt zwei Arten von Mitgliedern:
   1. ordentliche Mitglieder, die sich nach Absprache im Verein Angestöpselt engagieren und einen Mitgliedsbeitrag gemäß Beitragsordnung zahlen.
   2. Fördermitglieder, die nicht aktiv an den Aktivitäten des Vereins teilhaben und die einen selbst gewählten, regelmäßigen Beitrag zahlen. Der Beitrag muss über dem Regelbeitrag für ordentliche Mitglieder liegen.
      {type=a}
6. Zeichnen sich ordentliche Mitglieder durch überdurchschnittliches Engagement für den Verein Angestöpselt aus, so können diese auf Antrag durch den Vorstand von den Mitgliedsbeiträgen befreit werden. Eine Freistellung kann frühestens nach Beendigung des laufenden Vereinsjahrs erfolgen.
7. Die Mitgliedschaft beginnt mit Eingang des Antrags und der Überweisung des Mitgliedsbeitrags für das laufende Vereinsjahr.
8. Entstehen Zahlungsrückstände eines Mitgliedes ist er bis zur vollständigen Begleichung des Rückstandes von den Vereinsleistungen und des Stimmrechtes ausgeschlossen.
9. Der Vereinszweck wird ausschließlich durch Beiträge und Spenden finanziert.

## §&nbsp;6 Ende der Mitgliedschaft

1. Die Mitgliedschaft endet durch Austritt, Ausschluss oder Tod.
2. Die Dauer der Mitgliedschaft ist durch Vertrauen geprägt. Die Kündigung der Mitgliedschaft kann jederzeit, schriftlich, ohne Angaben von Gründen und mit zwei Wochen zum Ende eines Quartals erfolgen. Die Mitgliedschaft endet dann mit Ablauf des entsprechenden Quartals. Eine Rückzahlung ggf. bereits geleisteter Beiträge erfolgt nicht.
3. Mitglieder des Vereins, die ihren Verpflichtungen nicht nachkommen oder in sonstiger Weise den Vereinsinteressen grob zuwiderhandeln, können durch den Vorstand mit einfacher Mehrheit ausgeschlossen werden. Gleiches gilt, wenn ein sonstiger wichtiger Grund vorliegt. Gegen den Beschluss des Vorstandes kann der/die Betroffene binnen eines Monats nach Mitteilung des Vorstandsbeschlusses schriftlich Einspruch einlegen. Über den Einspruch entscheidet die Mitgliederversammlung.

## §&nbsp;7 Organe

Organe des Vereins sind die Mitgliederversammlung und der Vorstand.

## §&nbsp;8 Vorstand

1. Der Vorstand besteht aus mindestens zwei und höchstens drei Personen. Der Vorstand kann nur aus zwei Personen bestehen, wenn keine dritte Person das Amt annehmen möchte oder diese bei der Wahl nicht die Mehrheit der Mitgliederstimmen hat. Ein Vorstandsmitglied ist dabei der Vorsitzende, welcher von der Mitgliederversammlung gewählt wird.
2. Der Vorstand leitet den Verein und führt seine Geschäfte. Zur gerichtlichen und außergerichtlichen Vertretung des Vereins im Sinne des §&nbsp;26 BGB sind jeweils zwei Vorstandsmitglieder vertretungsberechtigt.
3. Die Vorstandsmitglieder werden von der Mitgliederversammlung auf die Dauer von drei Jahren gewählt. Sie bleiben bis zur Wahl des nächsten Vorstandes im Amt. Eine Wiederwahl ist möglich. Scheidet ein Vorstandsmitglied während der Wahlperiode aus, hat der Vorstand unverzüglich ein Ersatzmitglied zu kooptieren, das bis zur nächsten Neuwahl im Amt bleibt.
4. Die Mitglieder des Vorstandes erhalten für ihre Tätigkeit grundsätzlich keine Vergütung.

## §&nbsp;9 Mitgliederversammlung

1. Befugnisse der Mitgliederversammlung
   1. Die Angelegenheiten des Vereins werden, soweit sie nicht vom Vorstand zu besorgen sind, durch die Versammlung der Mitglieder geordnet. Die Mitgliederversammlung wählt die Vorstandsmitglieder gem. §&nbsp;7 Abs. 3. Sie beschließt über die Höhe der Mitgliedsbeiträge gem. §&nbsp;4 Abs. 3a, über den Einspruch eines vom Vorstand ausgeschlossenen Mitglieds gem. §&nbsp;5 Abs. 3, über Satzungsänderungen gem. §&nbsp;8 Abs. 7 und die Auflösung des Vereins gem. §&nbsp;12.
   2. Der Vorstand hat der Mitgliederversammlung nach Abschluss des Vereinsjahres einen Geschäftsbericht zu erstatten und ihr die Jahresrechnung vorzulegen. Die Mitgliederversammlung beschließt über die Entlastung des Vorstandes.
   3. Die Mitgliederversammlung wählt für die Dauer von drei Jahren eine/n Kassenprüfer/in. Diese/r darf nicht Mitglied des Vorstands sein. Wiederwahl ist zulässig.
      {type=a}
2. Die ordentliche Mitgliederversammlung wird mindestens einmal im Jahr abgehalten. Die Einberufung der Mitglieder erfolgt durch schriftliche Einladung des Vorstandes unter Einhaltung einer Frist von drei Wochen. Einladungen auf elektronischem Weg sind zulässig. Das Einladungsschreiben gilt als den Mitgliedern zugegangen, wenn es an die letzte dem Verein bekannt gegebene Adresse gerichtet war. Mit der Einladung ist die Tagesordnung bekannt zu geben. Die Mitgliederversammlung wird von einem Mitglied des Vorstandes geleitet.
3. Die Mitgliederversammlung ist ohne Rücksicht auf die Zahl der erschienenen Mitglieder beschlussfähig, worauf in der Einladung hinzuweisen ist.
4. Jedes Mitglied hat eine Stimme. Es kann sich mit schriftlicher Vollmacht durch ein anderes Mitglied vertreten lassen. Dabei kann ein Mitglied höchstens drei weitere Mitglieder vertreten.
5. Die Abstimmung in der Mitgliederversammlung erfolgt durch Handaufheben, falls nicht von mindestens 1/3 der erschienenen Mitglieder Abstimmung durch Stimmzettel verlangt wird.
6. Zur Beschlussfassung genügt die einfache Mehrheit der anwesenden bzw. vertretenen stimmberechtigten Mitglieder. Bei Stimmengleichheit gibt die Stimme des Vorsitzenden den Ausschlag.
7. Beschlüsse auf Änderung der Satzung bedürfen einer Mehrheit von 2/3 der erschienenen bzw. vertretenen stimmberechtigten Mitglieder.
8. Über Beschlüsse der Mitgliederversammlung ist eine Niederschrift zu fertigen, die von dem/der Versammlungsleiter(in) zu unterzeichnen ist. Diese wird allen Mitgliedern zugestellt.

## §&nbsp;10 Aufgaben

1. Die Mitglieder des Vorstands tragen die Gesamtverantwortung für die Führung des Vereins. Der Vorstand überträgt seinen Mitgliedern im Innenverhältnis die nachfolgenden Aufgaben:
   1. Kassenwart
   2. Schriftführer/in
      {type=a}
2. Die Aufgaben werden in Abstimmung mit dem Vorstand auf unbestimmte Zeit ausgeführt.

## §&nbsp;11 Anträge

Anträge an die Mitgliederversammlung aus der Reihe der Mitglieder sind mindestens 7 Tage vor Zusammentritt der ordentlichen Mitgliederversammlung dem Vorstand schriftlich mit kurzer Begründung einzureichen. Bei Dringlichkeitsanträgen, über deren Zulassung die Vorstandsmitglieder mit einfacher Mehrheit entscheiden, entfällt dieses Erfordernis.

## §&nbsp;12 Außerordentliche Mitgliederversammlung

Außerordentliche Mitgliederversammlungen sind einzuberufen, wenn es das Interesse des Vereins erfordert oder wenn die Einberufung von mindestens 1/10 der Vereinsmitglieder schriftlich unter Angabe der Gründe vom Vorstand verlangt wird. Für die außerordentliche Mitgliederversammlung gelten die Bestimmungen über die ordentliche Mitgliederversammlung entsprechend.

## §&nbsp;13 Auflösung

1. Zur Auflösung des Vereins bedarf es eines Beschlusses, an dem mindestens 2/3 der Mitglieder mitwirken, von denen mindestens 3/4 für die Auflösung stimmen.
2. Ist die erste Mitgliederversammlung nicht beschlussfähig, so entscheidet im Abstand von mindestens einer Woche eine erneute Mitgliederversammlung mit 3/4 Mehrheit der erschienenen Mitglieder.
3. Bei Auflösung des Vereins oder sonstiger rechtlicher Beendigung sowie bei Wegfall des mildtätigen oder gemeinnützigen Zwecks fällt das Vereinsvermögen an eine durch den Vorstand bestimmte gemeinnützige, mildtätig anerkannte Organisation die es ausschließlich für gemeinnützige bzw. mildtätige Zwecke zu verwenden hat.
4. Als Liquidatoren wird der Vorstand bestellt.
