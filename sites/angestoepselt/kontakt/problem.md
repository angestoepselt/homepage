---
layout: layouts/page.njk
useForms: true
---

# Hilfe bei Problemen mit unseren Computern

<form method="post" action="/kontakt/problem">

Falls du in irgendeiner Form Probleme mit den von uns ausgestellten Geräten
haben solltest, kannst du uns auf dieser Seite davon berichten. Wir werden uns
anschließend bei dir melden und versuchen gemeinsam, eine Lösung zu finden. Wir
bitten dich zunächst um ein paar Kontaktdaten zu deiner Person.

<label class="form-input">
  <span>Name:</span>
  <input type="text" name="contactname" required placeholder="Vorname Nachname" />
</label>

<label class="form-input">
  <span>Email:</span>
  <input type="email" name="contactemail" required placeholder="mail@beispiel.de" />
</label>

Alle von uns ausgegebene Rechner haben eine Nummer. Wenn du diese angibst,
können wir das Problem besser eingrenzen und schneller eine Lösung finden. Falls
es sich bei dieser Anfrage nicht um einen Computer, sondern um ein anderes Gerät,
handelt kannst du das Feld leer lassen. Beschreibe bitte im Anschluss das
Problem möglichst präzise.

<label class="form-input">
  <span>Rechnernummer:</span>
  <input type="text" name="reference" placeholder="ABC123" />
</label>

<label class="form-input">
  <span>Nachricht:</span>
  <textarea name="message"></textarea>
</label>

<!-- FORM -->

<div class="form-submit">
  <input type="submit" value="Abschicken" />
</div>

</form>
