---
layout: layouts/page.njk
useForms: true
eleventyNavigation:
  key: Kontakt
  order: 900
---

{% banner
  "",
  "contact-banner.jpg",
  "Aufnahme des Vereinsheims bei Nacht. Die Innenbeleuchtung ist an und der Angestöpselt-Schriftzug ist zu sehen."
%} {% endbanner %}

# Kontakt zu uns aufnehmen

Schön, dass du dich für unsere Arbeit interessierst! Bitte wähle unten eine
Kategorie aus, um direkt zum entsprechenden Kontaktformular zu gelangen. So
können wir deine Anfrage besser zuordnen. Du erreichst uns natürlich auch
postalisch oder per E-Mail bei den unten angegebenen Adressen – oder ganz
klassisch in Person bei uns [im Verein](#so-kommst-du-zu-uns).

- [Ich möchte einen (oder mehrere) _Computer beantragen_](/computer-beantragen)
- [Ich möchte _Hardware spenden_](/hardware-spenden)
- [Ich habe ein _Problem mit einem Computer_, den ich von Angestöpselt habe](/kontakt/problem)
- [Ich möchte mich ehrenamtlich im Verein _engagieren_](/mitmachen)
- [Ich habe ein _anderes Anliegen_](#andere-anliegen)

{.form-choices}

## Andere Anliegen

Bei anderen Angelegenheiten kannst du uns über dieses allgemeine Formular
erreichen. Bitte sieh aber zunächst oben in der Liste nach, weil wir für viele
Themen bereits entsprechende Kontaktmöglichkeiten bereitstellen. Solltest du
Fragen haben, freuen wir uns natürlich trotzdem hier auf deine Nachricht.

<form method="post" action="/kontakt">

<label class="form-input">
  <span>Name:</span>
  <input type="text" name="contactname" required placeholder="Vorname Nachname" />
</label>

<label class="form-input">
  <span>Email:</span>
  <input type="email" name="contactemail" required placeholder="mail@beispiel.de" />
</label>

<label class="form-input">
  <span>Nachricht:</span>
  <textarea name="message"></textarea>
</label>

<!-- FORM -->

<div class="form-submit">
  <input type="submit" value="Abschicken" />
</div>

</form>

## So kommst du zu uns

Du findest uns in der _Zeller Straße 29/31 in 97082 Würzburg_ ([Kartenansicht](https://www.openstreetmap.org/node/6620016025){target="_blank" rel="noopener noreferrer"}). Wenn du einen Computer willst, stelle bitte zuerst einen [Antrag](/computer-beantragen/privat). Sobald wir einen Computer für dich haben, schreiben wir dir eine E-Mail. Dann kannst du zur Abholung vorbeikommen.

## Öffnungszeiten

In der Regel sind wir Montags von 18:00 Uhr - 20:00 Uhr für Euch da.

### Zu Fuß oder mit den öffentlichen Verkehrsmitteln

Von der _Haltestelle Rathaus_ (Linien 1, 3, 4 und 5) lauft ihr über die Alte Mainbrücke und dann den Berg hoch zu uns (5-10 Minuten). Ihr könnt auch mit der Straßenbahn Nummer 2 oder 4 in Richtung Zellerau bis zur _Haltestelle Wörthstraße_ fahren. Von dort aus erreicht ihr uns auch in 5-10 Minuten.
Alternativ könnt Ihr auch mit den Buslinien 7, 420, 421, 426, 550/554/555, 8067, 8068, 8091 oder 8112 zur _Haltestelle Alte Mainbrücke_ fahren und in ca. 3 Minuten zu uns laufen.

### Mit dem Auto

Die Zeller Straße ist seit 2021 Einbahnstraße und nur noch von der Alten Mainbrücke kommend zu befahren. Ihr könnt _gegenüber in der Gasse zum Ein- und Ausladen_ halten. Ihr könnt _nicht_ direkt vor der Tür auf dem Fahrradweg parken (absolutes Halteverbot).
