---
layout: layouts/page.njk
extraStylesheets: ['finish']
---

# Es hat geklappt!

<svg class="finish-hero" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 214.46961 61.600083">
  <linearGradient id="finish-hero-stroke-gradient" class="stroke-gradient" gradientUnits="userSpaceOnUse">
    <stop offset="0%" />
    <stop offset="25%" />
    <stop offset="50%" />
    <stop offset="100%" />
  </linearGradient>
  <path
    class="cable"
    d="M0 30.29a.5.5 0 0 0-.5.5.5.5 0 0 0 .5.5h111.46a.5.5 0 0 0 .5-.5.5.5 0 0 0-.5-.5z"
    stroke="url(#finish-hero-stroke-gradient)"
  />
  <path
    class="contacts"
    d="M204.96 27.13h9.51m-9.51-7.32h9.51m-9.51 14.63h9.51m-9.51 7.32h9.51"
    stroke="url(#finish-hero-stroke-gradient)"
  />
  <path
    class="plug"
    d="M127.92 38.69h6.32a1 1 0 0 1 .94 1v11.45a1.07 1.07 0 0 0 .28.73l8.07 8.93a.93.93 0 0 0 .67.3h31.19a2.38 2.38 0 0 0 1.14-.28l4.14-3.61a.86.86 0 0 1 1.1.25l1.58 3.3a.91.91 0 0 0 .71.34h11.17a.92.92 0 0 0 .82-.52l3.89-7.46a2.3 2.3 0 0 0 .28-1.12l.11-44.47a1.07 1.07 0 0 0-.2-.63l-4.18-6a.92.92 0 0 0-.75-.4h-51a.93.93 0 0 0-.67.3l-8.07 8.93a1.07 1.07 0 0 0-.28.73V21.6a1 1 0 0 1-.94 1h-6.32m53.22 16.41a3.43 3.43 0 0 1-.86 2.27l-1.35 1.51a3.41 3.41 0 0 1-2.54 1.14h-19a3.45 3.45 0 0 1-2.54-1.1l-1.35-1.49a3.39 3.39 0 0 1-.87-2.27V21.92a3.39 3.39 0 0 1 .87-2.27l1.35-1.51A3.45 3.45 0 0 1 157.4 17h19a3.41 3.41 0 0 1 2.54 1.14l1.35 1.51a3.43 3.43 0 0 1 .86 2.27z"
    fill="url(#finish-hero-stroke-gradient)"
    stroke="url(#finish-hero-stroke-gradient)"
  />
</svg>

**Vielen Dank**&emsp;&mdash;&emsp;
Wir haben deine Anfrage erhalten und melden uns bei dir.
Du erhältst in Kürze eine Bestätigung per E-Mail.
Wenn du _keine E-Mail_ bekommst, prüfe bitte deinen Spam-Ordner. Kontrolliere auch, ob du die richtige E-Mail-Adresse angegeben hast.
