---
layout: layouts/page.njk
useForms: true
---

{% banner
  "",
  "work-banner.jpg",
  "Aufnahme einen Spendenbox auf einem Tisch mit Infomaterial von Angestöpselt."
%}
{% endbanner %}

# Geld spenden

Wir arbeiten alle ehrenamtlich. Wir verdienen kein Geld bei Angestöpselt. Wir machen die Arbeit in unserer Freizeit.
Damit wir möglichst vielen Menschen mit unserem Angebot helfen können brauchen wir deine Unterstützung.

Wir verwenden das Geld für spannende und langweilige Sachen:

- Einzelteile nachkaufen. Zum Beispiel SSDs oder RAM - spannend
- Zubehör kaufen. Zum Beispiel Webcams oder Headsets - spannend
- Serverinfrastruktur verbessern. Zum Löschen und Installieren der Computer - definitiv spannend
- Miete zahlen - langweilig
- Strom, Gas, Telefon, Internet, Bürobedarf, Versicherungen - langweilig aber definitiv notwendig

Ein aktueller Überblick über unsere monatlichen laufenden Kosten:

- Miete 840€
- Ökostrom/-gas 89€
- Bürobedarf 50€
- Internet 33€
- Versicherungen 30€

## Spendenkonto

Wir freuen uns über jede Spende. Falls ihr eine Spendenbescheinigung benötigt (siehe unten), schreibt bitte Name, Vorname und Adresse in den Betreff. Danke.

Inhaber: angestöpselt e.V. - Verein für Digitalkompetenz <br>
Bank: Sparkasse Mainfranken Würzburg<br>
IBAN: DE23 7905 0000 0047 3098 28<br>
BIC: BYLADEM1SWU

![EPC QR-Code für Spende](/assets/epc.png)

### Spendenbescheinigung

Angestöpselt e.V. ist vom Finanzamt Würzburg als mildtätiger Verein nach §&nbsp;51 ff. AO anerkannt. Für Spenden können wir deshalb gerne eine entsprechende Bescheinigung ausstellen.

- Beträge bis 300 Euro: Bitte beachte, dass bis zu einer Höhe von 300 Euro ein vereinfachter Nachweis (Kontoauszug) für das Finanzamt ausreichend ist. Auf Wunsch stellen wir dir natürlich trotzdem eine Spendenbescheinigung aus. Gib dafür bitte Name, Vorname und Adresse in der Betreffzeile deiner Überweisung an.
- Beträge über 300 Euro: Wir schicken dir in jedem Fall eine Spendenbescheinigung zu. Solltest du keine Bescheinigung erhalten, nimm bitte mit uns [Kontakt](/kontakt/#andere-anliegen)

## Fördermitgliedschaft

Wir brauchen Monat für Monat Geld. Du möchtest Monat für Monat Gutes tun? Yeah, it's a match.

Angestöpselt lebt von der Unterstützung jedes Einzelnen. Werde [Mitglied](/#) und helfe dabei, möglichst vielen Menschen digitale Teilhabe zu ermöglichen.
Dein Mitgliedsbeitrag an uns ist natürlich auch - genauso wie eine Spende - von der Steuer absetzbar.

## Melde dich!

Hast du weitere Fragen? Möchtest du ein konkretes Projekt unterstützen? Schreibe uns gerne eine Nachricht. Alternativ kannst du auch einfach mal im Verein (Zeller Str. 29, 97082 Wü // zu unseren [Öffnungszeiten](/kontakt/#so-kommst-du-zu-uns)) oder zu einer unserer Veranstaltungen vorbeikommen!

<form method="post" action="/spenden">

<label class="form-input">
  <span>Name:</span>
  <input type="text" name="contactname" required placeholder="Vorname Nachname" />
</label>

<label class="form-input">
  <span>Email:</span>
  <input type="email" name="contactemail" required placeholder="mail@beispiel.de" />
</label>

<label class="form-input">
  <span>Nachricht:</span>
  <textarea name="message"></textarea>
</label>

<!-- FORM -->

<div class="form-submit">
  <input type="submit" value="Abschicken" />
</div>

</form>
