---
layout: layouts/page.njk
---

# Da passt was nicht…

**Entschuldigung** &emsp;&mdash;&emsp;
Leider konnten wir dein Formular nicht verarbeiten.
Stelle bitte sicher, dass du alle Felder korrekt ausgefüllt hast.

Außerdem darf dein Browser Cookies nicht blockieren.
Das kann auch durch einen Werbeblocker passieren – eventuell musst du für unsere Seite eine Ausnahme hinterlegen.
Wir verwenden Cookies nur für die Sicherheit unserer Formulare und binden weder Werbung noch Tracking-Dienste ein.

Wenn das alles nicht hilft, schreibe uns bitte an [kontakt@coderdojo-wue.de](mailto:kontakt@coderdojo-wue.de).
