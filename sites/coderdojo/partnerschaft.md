---
layout: page
tags:
	- legal
eleventyNavigation:
  key: Partnerschaft
  order: 40
extraStylesheets: ['timeline']
contentClass: expand
---

# Ausrichten eines CoderDojo mit uns

Wir sind stets auf der Suche nach motivierten Partnern im Würzburger Umkreis, die zusammen mit uns ein CoderDojo ausrichten möchten. Hierbei kann es sich um Unternehmen aus der Privatwirtschaft handeln, aber auch um Vereine und öffentliche Institutionen.

Um den Einstieg zu erleichtern haben wir auf dieser Seite alle wichtigen Informationen über unsere Veranstaltungen zusammengestellt. Sie sollen helfen, einen Überblick über uns und CoderDojos im Allgemeinen zu erhalten.

Größtenteils beruht das hier beschriebene auf dem offiziellen [CoderDojo-Handbuch](https://raspberrypi.my.site.com/cdkb/s/article/The-CoderDojo-Champions-Handbook), unserem Erfahrungsschatz, den wir seit 2017 kontinuierlich verfeinern und dem Austausch mit anderen Ehren- und Hauptamtlichen in der Kinder- und Jugendarbeit im Bereich Technik und IT.

Natürlich stehen wir darüber hinaus gerne für Fragen bereit.
Falls ein CoderDojo ein Projekt ist, das anspricht, würden wir uns über eine Kontaktaufname freuen!

## Das Konzept CoderDojo

Das erste CoderDojo wurde 2011 in Irland ausgetragen. Seitdem hat es sich zu einer weltweiten Bewegung entwickelt, die inzwischen von der Raspberry Pi Foundation geleitet wird. Es wird geschätzt, dass momentan über 1250 Gruppen in über 69 Ländern CoderDojos veranstalten.

Die offizielle Mission von CoderDojo lautet:
> CoderDojo ist eine globale Bewegung kostenloser, offener,
durch Freiwillige organisierte Programmier-Clubs (Dojos), bei
denen Kinder und Jugendliche im Alter von 7–17 Jahren (Ninjas)
mit Unterstützung anderer Ninjas und freiwilliger Mentoren mehr über
digitale Technologien erfahren können. Die Mission von CoderDojo lautet, jungen
Leuten auf der ganzen Welt die Chance zu geben, das Programmieren in einem
sozialen und sicheren Umfeld zu erlernen.

Wir konkretisieren diese Mission in Würzburg indem wir folgende grundsätzliche Rahmenbedingungen festlegen:
* Inhaltlich geht es in unseren CoderDojos nicht nur ums Programmieren, sondern um alle Themen im Bereich IT, Medien und Medienkompetenz
* Unsere kostenlosen CoderDojos sind ganztägig, meistens von 9 bis 15 Uhr und enthalten eine Mittagspause
* In der Mittagspause gibt es eine kostenfreie Mahlzeit für alle Teilnehmende und Mentoren
* Eingeladen sind Kinder und Jugendliche von 9 bis 17 Jahren
* Unsere CoderDojos werden in den Räumlichkeiten von wechselnden Partnern ausgetragen, damit die Kinder (und auch die Eltern) unterschiedliche Unternehmen und Einrichtungen in der Region kennenlernen können und auch damit wir unsere Reichweite erhöhen können
* Es gibt keinen festen Rhythmus für CoderDojos in Würzburg; sie finden statt, wenn sie stattfinden
* All unsere Mentoren und anderen Helfer arbeiten vollständig ehrenamtlich und unentgeldlich

Grundsätzlich streben wir eine Partnerschaft an, in der wir die grobe Organisation, das Know-How und die Mentoren einbringen. Unsere Partner sollen die Räumlichkeiten, die Verpflegung und organisatorische Unterstützung vor Ort bereitstellen.

### Termine

Unsere Veranstaltungen finden meist an einem Samstag oder Sonntag statt. Das stellt zum einen sicher, dass wir den üblichen Betrieb eines Unternehmens oder einer Einrichtung nicht stören und zum anderen, dass genügend Mentoren und Helfer Zeit haben das CoderDojo auszurichten.

Während der Ferienzeit sind Termine unter der Woche auch denkbar, solange genügend Mentoren verfügbar sind.

### Räumlichkeiten

Für ein CoderDojo eignen sich alle gängigen Räumlichkeiten, in denen sonst auch Veranstaltungen stattfinden.
In erster Linie bedeutet das:

- Es muss (gerne auch über mehrere Räume hinweg) Platz für mindestens 30 Personen zur Verfügung stehen
- WLAN muss vorhanden sein und mind. 30 Personen gleichzeit versorgen können.
	Kabelgebundener Internetzugang ist für manche Workshops von Vorteil, aber kein Muss.
	Ggf. bringen wir auch entsprechende Hardware zur Umwandlung mit, sofern benötigt.
- Steckdosen und dazugehörige Mehrfachdosen müssen in ausreichender Anzahl vorhanden sein
- Die Möglichkeit (Tisch-)Gruppen bilden zu können, ist von Vorteil

### Teilnehmende

Wir erwarten Kinder und Jugendliche im Alter von 9 bis 17 Jahren. Diese melden sich entweder über unsere Webpräsenz oder in Absprache mit dem jeweiligen Partner über ein anderes datenschutzkonformes Portal für die Veranstaltung an.
Die Anmeldung dient der Planung, z. B. von Raumgrößen und der Verpflegung. Bei der Organistation über unsere Webseite stellen wir dem Veranstalter die nötigen Infos zur Teilnehmerzahl zur Verfügung.

Wir erlauben grundsätzlich, dass Eltern bei der Veranstaltung awesend sein dürfen, müssen sie aber nicht. Üblicherweise geht die Aufsichtspflicht für den Zeitraum der Veranstaltung auf den Veranstalter und die Mentoren über.

Eltern sind prinzipiell nicht für das Mittagessen eingeplant. Hier kann unser Partner entscheiden, ob für anwesende Eltern ebenfalls eine Verpflegung gestellt wird. 

### Mentoren

Vor jeder Veranstaltung fragen wir über unsere Kanäle uns bekannte Mentoren an, die Workshops ausrichten können. Meistens ist es uns damit möglich den Bedarf zu decken. Üblicherweise streben wir ein Verhältnis von einem Mentor zu 5 Kindern an.

Am besten ist es dennoch immer, wenn unser Partner ebenfalls Mentoren stellt. Diese können - sofern inhaltlich passend - die Arbeit im eigenen Unternehmen oder der Einrichtung vorstellen und so neue spannende Themen anbieten, die wir sonst nicht abdecken können.

### Workshops

Jeder Mentor bringt bei uns ein eigenes Thema im eingangs erwähnten Themenumfeld mit und bereitet es eigenständig vor. Meistens stehen eine Woche vor Beginn des CoderDojos die angebotenen Themen grob fest.
Wir versuchen die Workshops dann so zu planen, dass für alle Alters- und Vorwissensstufen inhaltlich etwas dabei ist. Notfalls bitten wir Mentoren ihre Themen zu wechseln, damit das passt.

Während der Veranstaltung hat jeder Mentor dann optimalerweise eine eigene Tischgruppe für sich und das jeweilige Thema.

Die Kinder können von Beginn an frei wählen, zu welchem Thema sie etwas lernen möchten und auch *jederzeit* das Thema wechseln. Das bedeutet, dass unsere Mentoren flexibel mit gehenden und neu dazukommenden Kindern umgehen können müssen.

Die Kinder erfahren erst am Tag des CoderDojos welche Themen angeboten werden. Wir kommunizieren das erst zu Beginn der Veranstaltung, um zum einen möglichst lange flexibel auf Mentorenausfälle reagieren zu können und zum anderen, um das Interesse der Teilnehmenden nicht schon frühzeitig auf spezifische Themen zu konzentrieren. Bei der Anmeldung soll das allgemeine Interesse an IT und Technik im Vordergrund stehen.

Beliebte Workshops und Workshops, die wir daher sehr oft anbieten sind z. B.:
* Programmierung mit Scratch
* 3D-Druck
* PC-Hardware (Erklärung Komponenten und Zusammenbau)
* Eine eigene Webseite bauen

### Hardware

Teilnehmende sind aufgefordert, nach Möglichkeit eigene Laptops oder Tablets zum CoderDojo mitzubringen.

Das ist nicht immer allen möglich, daher sind wir immer froh, wenn am Veranstaltungsort z. B. Tagungslaptops verwendet werden können. Das ist aber kein muss.

In begrenzter Zahl können wir für manche Workshops Hardware vom Verein bereitstellen.
Unserer Erfahrung nach ist es aber auch kein Problem (und manchmal sogar förderlich), wenn sich Gruppen um ein Gerät bilden und im Team gearbeitet wird.

### Fotos

Während den CoderDojos machen wir normalerweise Fotos für unsere Webseite.
Wir fragen bei der Anmeldung nach, ob Fotos von den Kindern angefertigt werden dürfen.
Kinder die -- bzw. Kinder, deren Eltern -- das verneinen werden von uns entsprechend mit auffälligen Aufklebern markiert, sodass sie nicht fotografiert werden.

Die Bildrechte beinhalten ausschließlich das Hochladen auf unsere Webseite und die Webseite des Veranstalters. Es ist nicht möglich die Bilder auf Social Media Plattformen hochzuladen.

Der Veranstalter darf natürlich ebenfalls Fotos anfertigen, sofern die genannten Bedingungen eingehalten werden.

### Werbung

Wir bewerben jedes CoderDojo über unsere Kanäle, insbesondere über unseren Newsletter.
Gleichzeitig freuen wir uns, wenn unsere Partner über ihre eigenen Kanäle uns, und vor allem das konkrete Event, bekannt machen.

Weiterhin bitten wir darum, ggf. an der Veranstaltung selbst Werbung für weitere CoderDojos und den Verein Angestöpselt machen zu dürfen.
Das erlaubt uns, das Projekt auch langfristig am Leben zu halten.

Im Gegenzug platzieren wir die Namen und die Logos unserer Partner auf unserer Webseite, nennen sie im Newsletter und bei Werbung über unsere Kanäle.
Zusätzlich ist das CoderDojo selbst eine Gelegenheit, die eigenen Räumlichkeiten, Fähigkeiten und Mitarbeiter zu präsentieren, um damit Eltern und letztlich auch unsere jungen Teilnehmer als potenzielle Bewerber und/oder Kunden zu gewinnen.

## Ablauf

Im Groben läuft ein CoderDojo wie folgt ab:

{% timeline "👋" %}

### Begrüßung

Zu Beginn versammeln wir alle Kinder, Mentoren, Helfer und (noch) anwesende Eltern in einem Raum.

Wir beginnen mit einer kurzen Begrüßung und Vorstellungsrunde. Wir stellen dabei zunächst CoderDojos selbst kurz für alle Kinder vor, die zum ersten Mal dabei sind. Der Veranstalter hat hier auch die Gelegenheit sich vorzustellen. Im Anschluss folgen organisatorische Informationen zu den Räumlichkeiten, den Toiletten und zur Verpflegung. Zu guter Letzt stellen die Mentoren sich und ihre Themen selbst vor, damit die Kinder wissen, an welchen Tischgruppen welche Themen angeboten werden.

Üblicherweise ist das Format dabei so informell und kurz wie möglich. Das bedeutet, dass wir soweit möglich auf Präsentationen oder andere starre Mittel verzichten. Die Begrüßung dauert normalerweise maximal 10 Minuten.

Die Kinder können nun frei zu den Themen gehen, die sie interessieren. Durch die Möglichkeit jederzeit zu wechseln, können sie selbst entscheiden, ob ein Thema für sie funktioniert oder nicht. Die Mentoren sind angehalten hier nicht zu intervenieren (weder zum Bleiben, noch zum Gehen).

{% endtimeline %}
{% timeline "🍕" %}

### Mittagessen

Sobald das Mittagessen bereit ist (normalerweise gegen 12 Uhr), rufen wir alle Kinder zusammen, damit sie gemeinsam zu Mittag essen und eine Pause machen. Die Mentoren sind angehalten die Kinder in ihrer Gruppe zum Essen zu schicken und ebenfalls etwas zu essen.

Wir geben für die Pause keine feste Zeit vor. Die Kinder können frei entscheiden, ab wann sie wieder an einem Thema arbeiten wollen. Wir achten nur darauf, dass sie nicht arbeiten, während sie essen.

{% endtimeline %}
{% timeline "⏰" %}

### Abschluss

Am Ende versammeln wir wieder alle Kinder, Mentoren und (wieder) anwesende Eltern in einem Raum.

Jeder Mentor *kann* nun die Ergebnisse der eigenen Tischgruppe vorstellen oder durch die Kinder (wenn sie das möchten) vorstellen lassen. Dadurch bekommen auf der einen Seite die Kinder noch einmal die Bestätigung, dass das, was sie gelernt und gemacht haben von Bedeutung und gut war und auf der anderen Seite die Eltern einen Einblick in unsere Arbeit und in die Fähigkeiten ihrer Kinder.

Zu guter Letzt verabschieden wir alle Teilnehmenden gemeinsam mit dem Veranstalter und bitten einerseits um Feedback und anderseits um Spenden für unseren Verein, der ohne Spenden nicht überleben könnte.

{% endtimeline %}
