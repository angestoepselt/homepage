---
layout: layouts/page.njk
useForms: true
---

# CoderCamp-Umfrage

Wir überlegen zurzeit ein CoderCamp, eine Ferien- / Wochenendfreizeit im Stil des CoderDojo, anzubieten.
Es würde verschiedene Workshops zu IT- und nicht-IT-Themen geben, ein gemeinsames Übernachten, Spiele und vielleicht noch ein paar andere Programmpunkte.
Mitfahren kann Jede und Jeder im Alter zwischen 9 und 17, wir werden wahrscheinlich aber um einen Unkostenbeitrag für Übernachtung und Verpflgung nicht herumkommen.
Die kostenlosen CoderDojos werden natürlich weiterhin unabhängig davon geplant.

Hier sind wir auf eueren Input angewiesen!
Mit diesem Formular könnt ihr uns schon mal als ersten Anhaltspunkt ein bisschen Feedback geben, das wir bei der weiteren Planung berücksichtigen können.
Du kannst es entweder alleine oder mit Familie / Geschwistern zusammen ausfüllen.

<form method="post" action="/freizeit">

<input type="hidden" name="veranstaltung" value="06. Mai 2023" />

Bitte verrate uns deinen Namen und deine E-Mail-Adresse, damit wir bei Fragen auf dich zurück kommen können.

<label class="form-input">
  <span>Name:</span>
  <input type="text" name="contactname" required />
</label>

<label class="form-input">
  <span>Email:</span>
  <input type="email" name="contactemail" required placeholder="mail@beispiel.de" />
</label>

Wie viele Interssierte an solch einer Freizeit seid ihr, und wie alt?

<label class="form-input">
  <span>Interessierte von 9 bis 12 Jahren:</span>
  <input type="number" min="0" value="0" name="interested-kids" />
</label>

<label class="form-input">
  <span>Interessierte von 12 bis 15 Jahren:</span>
  <input type="number" min="0" value="0" name="interested-teens" />
</label>

<label class="form-input">
  <span>Interessierte ab 16 Jahren:</span>
  <input type="number" min="0" value="0" name="interested-youngadults" />
</label>

Welches Format würdest du / ihr bevorzugen?

<input type="radio" id="format-a" class="radio-input" name="format" required value="Wochenende" />
<div class="form-input">
	<label for="format-a">Ein Wochenende (gemeinsame Anreise am Freitag, Abreise am Sonntag).</label>
</div>
<input type="radio" id="format-b" class="radio-input" name="format" required value="Unter der Woche" />
<div class="form-input">
	<label for="format-b">Unter der Woche (während den Ferien, etwa 4 Tage inklusive An- und Abreise).</label>
</div>
<input type="radio" id="format-c" class="radio-input" name="format" required value="Egal" />
<div class="form-input">
	<label for="format-c">Ist mir egal.</label>
</div>

Möchtest du / möchtet ihr an den Übernachtungen teilnehmen (zum Beispiel in einer Jugendherberge)?

<input type="radio" id="overnight-a" class="radio-input" name="overnight" required value="Ja" />
<div class="form-input">
	<label for="overnight-a">Ja.</label>
</div>
<input type="radio" id="overnight-b" class="radio-input" name="overnight" required value="Nein" />
<div class="form-input">
	<label for="overnight-b">Nein, ich würde lieber Abends wieder nach Hause.</label>
</div>

Hast du noch weitere Ideen, die uns helfen könnten?
Oder möchtest du (das geht in erster Linie an die Erwachsenen) als Mentor / Mentorin oder im Organisationsteam mithelfen?
Dann schreib uns gerne hier!

<label class="form-input">
  <span>Nachricht:</span>
  <textarea name="message"></textarea>
</label>

Das war's!
Wir hoffen wirklich sehr, so eine Veranstaltung auf die Beine gestellt bekommen.
Versprechen können wir natürlich noch nichts&mdash;aber mit dieser Umfrage wollen wir uns schon mal ein Bild davon machen, wie groß das Interesse ist.

<!-- FORM -->

<div class="form-submit">
  <input type="submit" value="Anmeldung abschicken" />
</div>

</form>

