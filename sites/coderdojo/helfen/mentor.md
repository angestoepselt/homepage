---
layout: page
---

# Mentor werden

Du hast Interesse an Coding und Technik im Allgemeinen und möchtest dein Wissen teilen?
Du hast Lust, Kindern und Jugendlichen ehrenamtlich die Welt der IT näherzubringen?
Du möchtest dich mit Gleichgesinnten treffen und dich mit anderen aus der Branche unterhalten?
Du möchtest über dich hinaus wachsen und dich sozial engagieren?

**Dann werde ehrenamtlicher Mentor bei uns!**

Wir suchen Leute wie dich, die Lust haben, in einer freundlichen und kreativen Umgebung den nächsten Generationen Wissen zu vermitteln.

## Was muss ich tun, wenn ich bei euch Mentor werde?

Zunächst tragen wir dich in unserem Mailverteiler ein und schreiben dich ab dann an, sobald ein neues CoderDojo geplant ist.

Für ein CoderDojo bringt jeder Mentor ein eigenes Thema im Bereich IT, Medien und Medienkompetenz mit und bereitet es eigenständig vor.
Du entscheidest frei, bei welchen CoderDojos du dabei sein möchtest und auch welche Themen du anbietest.

Während der Veranstaltung hat jeder Mentor dann eine eigene Tischgruppe für sich und das jeweilige Thema.
Die Kinder und Jugendliche können von Beginn an frei wählen, zu welchem Thema sie etwas lernen möchten und auch *jederzeit* das Thema wechseln.

Mittags gibt es dann für alle Teilnehmer - und auch für dich - kostenlos Pizza!

Beliebte Workshops und Workshops, die wir daher sehr oft anbieten sind z. B.:
* Programmierung mit Scratch
* 3D-Druck
* PC-Hardware (Erklärung Komponenten und Zusammenbau)
* Eine eigene Webseite bauen

## Überzeugt?
Dann melde dich bei uns! Wir sind gespannt auf deine Themenideen!