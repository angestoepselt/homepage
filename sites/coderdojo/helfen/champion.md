---
layout: page
---

# Werde auch du zum Champion!

CoderDojo Champions sind unsere ehrenamtlichen Organisatoren, ohne die es keine CoderDojos in Würzburg geben würde.
Als Champion trägst du dazu bei, dass Kinder und Jugendliche kostenlosen Zugang zu unseren Bildungsangeboten erhalten.
Mit deinem organisatorischen Talent und deiner offenen Art hältst du alles zusammen und trägst die CoderDojos in die Region.

## Deine Aufgaben

Als ehrenamtliche/-r Organisator/-in sorgst du mit deinem Engagement und deiner Leidenschaft für die nächste Generation von Programmierern und Technikern.

### Dazu gehört:

- **Veranstaltungsort und Partner finden:**
  Wir müssen regelmäßig geeignete Veranstaltungsorte und Partner finden, die für unsere CoderDojos geeignet sind.
  Das kann beispielsweise ein Unternehmen, eine Schule, ein Gemeindezentrum oder eine Bibliothek sein.
  Du kümmerst dich um die Kommunikation mit unseren Partnern und knüpfst neue Kontakte.
- **Mentoren gewinnen:**
  Für jedes anstehende Dojo müssen wir Mentoren gewinnen, die bereit sind, ihre Fähigkeiten und ihr Wissen an die Kinder und Jugendlichen im CoderDojo weiterzugeben.
  Mentoren können beispielsweise Mitarbeiter unserer Partner, Studierende oder einfach erfahrene Programmierer in unserem Netzwerk sein.
- **CoderDojo bewerben:**
  Unsere CoderDojos wollen beworben werden, um möglichst viele Kinder und Jugendliche zu erreichen.
  Das kann durch unsere Newsletter, Social Media, lokale Presse oder Mundpropaganda erfolgen.
  Dabei helfen normalerweise auch unsere Partner.
- **CoderDojo Vorbereiten**:
  Wir müssen zusammen mit unseren Partnern vor Beginn der CoderDojos sicherstellen, dass alles, was benötigt wird, vorhanden ist.
  Dazu gehören z. B. WLAN, Mehrfachsteckdosen, Getränke und Mittagsverpflegung. 
- **CoderDojo durchführen**:
  Champions sind auch während den CoderDojos anwesend und spielen eine entscheidende Rolle -- sie sorgen für den organisatorischen Rahmen.
  Dazu gehört auch das Dojo offiziell zu starten und zu beenden: die Eltern und Kinder begrüßen, durch den Tag führen und am Ende auch wieder verabschieden.

## Reinschnuppern!

Du willst ganz unverbindlich erleben, wie ein CoderDojo abläuft und herausfinden, ob Champion sein etwas für dich wäre? 
Schaue doch am besten einfach bei einer unseren nächsten Veranstaltungen vorbei.
Melde dich gerne vorher bei uns, damit wir dir schon vorab ein wenig von unserer Arbeit zeigen können.
