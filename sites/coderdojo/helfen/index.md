---
layout: layouts/page.njk
useForms: true
eleventyNavigation:
  key: Helfen
  order: 30
---

# Hilf uns, das Würzburger CoderDojo voran zu treiben!

Wir freuen uns über alle, die mit uns gemeinsam das Projekt CoderDojo weiter führen wollen.
Sei es aus Interesse an der Technik, um Kinder und Jugendliche über die schulischen Inhalte hinaus für Technik zu begeistern, oder einfach nur, um neue Leute kennenzulernen und einen wertvollen Beitrag für die Gemeinschaft zu leisten.
Wir sind stets auf der Suche nach Verstärkung, um unsere CoderDojos in der Region Würzburg häufiger anbieten zu können. Die Nachfrage dafür ist da!

In erster Line kannst du uns (neben einer gern zweckgebundenen [Geldspende](https://angestoepselt.de/spenden)) wie folgt unterstützten:

- [Als _Mentor_ Workshops für Kids und Jugendliche durchführen](/helfen/mentor)
- [Als _Champion_ organisatorische Aufgaben übernehmen](/helfen/champion)

{.form-choices}

Um uns als Champion zu unterstützen brauchst du kein spezielles technisches Fachwissen. Ein Talent für Organisatorisches und kommunikative Fähigkeiten genügen!
Als Mentor kannst du eines (oder auch mehrere) technische Themen aufbereiten und Kindern und Jugendlichen im Rahmen eines unserer CoderDojos näher bringen.
Egal für welche Rolle du dich entscheidest: Wir sind natürlich mit unseren Erfahrungen aus vergangen Veranstaltungen mit dabei und unterstützen dich.

Klingt ansprechend?
Schreibe uns doch einfach an die unten angegebene Email.
Wir freuen uns auf deine ehrenamtliche Mitarbeit!
