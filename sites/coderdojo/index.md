---
layout: layouts/home.njk
headerTitle: CoderDojo
contentClass: expand
---

{% banner
  "CoderDojo Würzburg",
  "home-banner.jpg",
  "Kinder und Jugendliche verfolgen die Präsentation eines 3D-Druckers"
%}
Kostenlose Programmier- und Techniktreffen für Kinder und Jugendliche im Raum Würzburg.
{% endbanner %}

<section class="page-actions">
  <div>
    <h2>Aktuelles</h2>
    <p>
      Wir laden zu folgender Veranstaltung ein:
		</p>
    <ul>
      <li>Sa. 22.03.25, <a href="https://www.vr-bank-wuerzburg.de/privatkunden.html" target="_blank" rel="noopener noreferrer">VR@MOZ</a></li>
    </ul>
    <p>
      Die Anmeldung erfolgt <a href="https://login.vr-ticket.de/vr-bank-wuerzburg/coderdojo/" target="_blank" rel="noopener noreferrer">hier</a> über die VR-Bank Würzburg.
    </p>
  </div>

  <a href="https://login.vr-ticket.de/vr-bank-wuerzburg/coderdojo/">
    <h3>Jetzt anmelden</h3>
    <em class="inline-callout">Sa. 22.03., VR@MOZ</em>
  </a>

  <a href="https://mailing.angestoepselt.de/subscription/form" target="_blank">
    <h3>Newsletter</h3>
    <span>Bleibe über neue Veranstaltungen informiert.</span>
  </a>

  <a href="mailto:kontakt@angestoepselt.de">
    <h3>Kontakt</h3>
    <span>Wir suchen immer interessierte Kursleitende!</span>
  </a>
</section>

{% section %}

## Was ist ein CoderDojo?

CoderDojo ist eine weltweite Bewegung für kostenlose Programmiertreffen für junge Menschen, die von Ehrenamtlichen unterstützt wird.
Dort können sich Teilnehmende - egal ob mit Vorkenntnissen oder ohne - mit der digitalen Welt befassen.

### Was kann ich dort machen?

Wir haben eine Auswahl an verschiedenen Themen für Anfänger, Fortgeschrittene und Profis.
Unsere Mentorinnen und Mentoren helfen Dir und beantworten Dir alle Fragen.
Falls Du schon mehr Erfahrung hast oder eigene Ideen und Projekte mitbringen willst, kannst Du natürlich auch Dein eigenes Ding machen – wir unterstützen Dich dabei!

### Was muss ich sonst noch wissen?

Das Ganze ist komplett kostenlos, auch für Verpflegung ist gesorgt!
Bitte melde Dich aber vorher online an, damit wir besser planen können.
Falls Du einen eigenen Laptop hast, bringe ihn bitte mit.

{% endsection %}

{% section true %}

## Unsere Geschichte

Unser Ziel ist es, Begeisterung für Technik und Programmieren in Kindern und Jugendlichen zu wecken und damit Digitalkompetenz zu vermitteln.
Das 2011 in Irland gegründete [CoderDojo](https://coderdojo.com/) Projekt, welches inzwischen zur [Raspberry Pi Foundation](https://www.raspberrypi.org/) gehört, war der Startschuss für Veranstaltungen auf der ganzen Welt, die genau dieses Ziel verfolgen.
Seitdem sind CoderDojos auch überall in Deutschland entstanden - in Kooperationen mit diversen Unternehmen, die ihre Zeit, Räumlichkeiten und Mittel für die Verpflegung zur Verfügung stellen.

[Zu den bisherigen Veranstaltungen](/rueckblick){.cta-link}

{% endsection %}
