---
layout: page
tags:
  - legal
eleventyNavigation:
  key: Datenschutz
  order: 20
---

# Datenschutz

Allgemein gelten die [Datenschutzbestimmungen des Vereins Angestöpselt e.V.](https://www.angestoepselt.de/datenschutzhinweis/).

## Webseite

Diese Webseite wird vom Verein Angestöpselt e.V. betrieben.
In den Protokolldaten des Servers werden eventuell folgende Daten festgehalten:

- IP-Adresse
- User-Agent des Browsers

Diese Daten werden nicht an Dritte weitergegeben und höchstens für grobe Aufrufstatistiken verwendet.

## Veranstaltungen

Wenn Sie sich selbst oder Ihr Kind für eine unserer Veranstaltungen anmelden, erheben wir die folgenden personenbezogenen Daten über Sie bzw. Ihr Kind.
Dies geschieht durch das Ausfüllen und Einschicken des Formulars in Papierform oder online.
Die folgenden Angaben verwenden o.b.d.A. das generische Maskulin.

- Voller Name des anmeldenden Erziehungsberechtigten
- Voller Name des angemeldeten Kindes (Teilnehmer)
- Geburtsdatum des Teilnehmers
- Telefonnummer
- E-Mail-Adresse

Diese Daten werden von uns bis zum Ende der jeweiligen Veranstaltung verwahrt und nicht an Dritte weitergegeben.
Nach dem Ablauf der Veranstaltung werden diese datenschutzkonform gelöscht bzw. vernichtet.
Möchten Sie danach erneut eine Veranstaltung besuchen, werden wir Ihre Daten nochmals erheben.

Zu statistischen Zwecken wird die Anzahl der tatsächlich erschienenen Teilnehmer einer Veranstaltung dauerhaft gespeichert.

Weiterhin werden auf unseren Veranstaltungen möglicherweise Fotos aufgenommen und auf unserer Homepage veröffentlicht.
Wir veröffentlichen nur Bilder mit Einverständnis der Teilnehmerin bzw. des Teilnehmers, welches natürlich auch im Nachhinein mit einer kurzen, formlosen Nachricht an uns widerrufen werden kann.
