---
layout: layouts/page.njk
useForms: true
eleventyNavigation:
  key: Anmelden
  order: 20
---

# Für das CoderDojo anmelden

Die Anmeldung erfolgt <a href="https://login.vr-ticket.de/vr-bank-wuerzburg/coderdojo/" target="_blank" rel="noopener noreferrer">hier</a>.
