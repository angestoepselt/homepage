---
layout: layouts/page.njk
useForms: true
---

# LAN-Party, CoderDojo-Style!

Zum ersten Mal wollen wir eine coderParty veranstalten.
coderParty soll unser neues, unregelmäßiges Format werden, bei dem ihr in einer lockeren Atmosphäre mit uns ins Gespräch kommen könnt&ndash;und umgekehrt!

Die Veranstaltung wird online an einem Abend stattfinden.
Im Gegensatz zu unseren regulären CoderDojos bereiten wir keine Workshops oder andere Programmpunkte vor.
Stattdessen könnt ihr den Abend selbst mit Themen füllen.

Den Anfang machen wir mit einer Klassischen [LAN-Party](https://de.wikipedia.org/wiki/LAN-Party){target="_blank" rel="noopener noreferrer"} im Spiel [Minecraft](https://www.minecraft.net/de-de){target="_blank" rel="noopener noreferrer"}.

Hier noch die Eckdaten:

> **Datum**:&emsp;Samstag, ??. Juni 2023  
> **Ort**:&emsp;Online (du benötigst die Software [Mumble](https://www.mumble.info/){target="_blank" rel="noopener noreferrer"}, eine freie Alternative zu TeamSpeak oder Discord)  
> **Start**:&emsp;18:00 Uhr  
> **Ende**:&emsp;Open End (solange wir alle Lust haben)  

<form method="post" action="/anmelden">

<input type="hidden" name="veranstaltung" value="06. Mai 2023" />

Bitte verrate uns deinen Namen und deine E-Mail-Adresse, an die wir Informationen zur Veranstaltung schicken können:

<label class="form-input">
  <span>Name:</span>
  <input type="text" name="contactname" required />
</label>

<label class="form-input">
  <span>Email:</span>
  <input type="email" name="contactemail" required placeholder="mail@beispiel.de" />
</label>

Als Auftacktveranstaltung wollen wir zusammen Minecraft spielen.
Dazu benötigst du das Spiel, in der Java- oder Bedrock-Variante für PC oder Android / iOS (die Fassung für Nintendo Switch erlaubt leider nicht, auf fremde Server zu verbinden).
Falls du mitspielen möchtest (du kannst natürlich auch nur zum Gespräch kommen), bräuchten wir deinen Spielernamen:

<label class="form-input">
  <span>Java-Spielername:</span>
  <input type="text" name="javaname" />
</label>

<label class="form-input">
  <span>Bedrock-Spielername:</span>
  <input type="text" name="bedrockname" />
</label>

Hast du noch Fragen, Wünsche oder Anregungen?
Wir arbeiten noch an dem Format und freuen uns auch auf euer Feedback!

<label class="form-input">
  <span>Nachricht:</span>
  <textarea name="message"></textarea>
</label>

<!-- FORM -->

<div class="form-submit">
  <input type="submit" value="Anmeldung abschicken" />
</div>

</form>

## Warum Minecraft?

Weil sich das Spiel bei einigen der letzten CoderDojos bewährt hat.
Uns ist bewusst, dass Minecraft weder freie Software noch durch den kürzlich eingeführten Microsoft-Zwang datenschutzmäßig unbedinglich ist.
Gleichzeitig würden wir aber zum Ausprobieren dieses neuen Formats ein Thema wählen, für das bereits Interesse bekundet wurde.
Außerdem gefällt uns die Idee, eine LAN-Party zu veranstalten.
Und Minecraft läuft nun mal auf allen größeren Plattformen.

Wenn ihr Ideen für die nächsten coderPartys habt, schreibt uns ruhig!
