---
layout: page
tags:
  - legal
eleventyNavigation:
  key: Impressum
  order: 10
---

# Impressum

[CoderDojo](https://coderdojo.com/) ist ein weltweites Projekt, welches von
der [Raspberry Pi Foundation](https://www.raspberrypi.org/) vorangetrieben wird.
In Würzburg wird das Projekt unter der Schirmherrschaft des gemeinnützigen
Vereins [Angestöpselt e. V.](https://www.angestoepselt.de/) durch Maximilian
Gutwein und Yannik Rödel geleitet.

> **Kontakt zur Projektleitung**: <kontakt@coderdojo-wue.de>

## Angaben gemäß § 5 TMG

<address>

**Angestöpselt e.V.**  
Zeller Straße 29/31  
97082 Würzburg

</address>

**Vertreten durch**: Lukas Seeber, Matthias Hemmerich und Tobias Benra

**Telefon**: [0931 66397812](tel:+4993166397812)  
**E-Mail**: <info@angestoepselt.de>

Siehe dazu auch [hier](https://www.angestoepselt.de/impressum/).

## Bilder und Fotos

> **Fotografien der Veranstaltungen**: Maximilian Gutwein, iWelt AG, Alexander
> Uder

Die Urheberrechte der veröffentlichten Bilder liegen bei den jeweiligen
Fotografen bzw. Abgebildeten - alle Rechte vorbehalten.

Bei der Anmeldung der Teilnehmer unserer Veranstaltungen wird abgefragt, ob die
teilnehmenden Kinder und eventuelle Begleitpersonen fotografiert und die Bilder
anschließend veröffentlicht werden dürfen.
Wir geben unser Bestes, niemanden widerwillig zu fotografieren.
Sollten Sie dennoch Ihr Bild oder das Ihrer Kinder auf unserer Seite finden und dies nicht (mehr) wünschen, kontaktieren Sie uns bitte.

## Haftung für Inhalte

Hier eingestellte Inhalte sind nach unserem besten Wissen und Gewissen auf Richtigkeit und Gesetzestreue geprüft.
Sollte Ihnen beim Besuch unserer Seiten etwas auffallen, wären wir über eine Nachricht dankbar, damit wir den Internetauftritt schnell anpassen können.
Bei Bekanntwerden von Rechtsverletzungen werden wir die entsprechenden Inhalte umgehend entfernen.

## Haftung für Links

Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben.
Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen.
Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich.
Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft.
Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar.
Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar.
Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.
