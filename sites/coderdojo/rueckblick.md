---
layout: layouts/page.njk
eleventyNavigation:
  key: Veranstaltungen
  order: 100
extraStylesheets: ['timeline']
contentClass: expand
---

# Unsere bisherigen Veranstaltungen

Seit dem ersten sehr erfolgreichen CoderDojo in Würzburg in 2017 gibt es sie auch in Würzburg in regelmäßigen Abständen.
Das Projekt steht unter der Schirmherrschaft von [Angestöpselt](https://www.angestoepselt.de/), einem gemeinnützigen Verein, der es sich zum Ziel gemacht hat, den Zugang zur digitalen Welt für alle zu ermöglichen, oder zumindest zu erleichtern.
Über das Schwesterprojekt Computerspende können Bedürftige Rechner und Peripherie, die wir als Spende erhalten, gratis beantragen.

Nach und nach knüpfen wir immer mehr Kontakte zu Unternehmen in Würzburg, die mitmachen wollen und ein CoderDojo anbieten möchten.
Inzwischen sind wir in fast jeder Ecke in und um Würzburg vertreten und können CoderDojos in einem Intervall von etwa 2 Monaten anbieten.

{% timeline "17" %}

### Juni 2023 <time datetime="2023-06-24">24. Juni 2023</time>

Die [Montesorri-Schule Würzburg](https://www.montessori-wuerzburg.de/){target="_blank" rel="noopener noreferrer"} richtet zum ersten Mal ein CoderDojo in Würzburg aus!

[
  ![Zwei Teilnehmende stehen in einem Flur und steuern eine kleine Drohne. Durch den Flur geht außerdem am Boden ein Kabel.](https://photos.gutwe.in/uploads/medium/d5/ef/1ee44c25fc928ee3777503c4040a.jpg){loading="lazy"}
  ![In einem etwas abgedunkeltem Zimmer sind türkise Tische aufgebaut. Knapp über zehn Kinder und Jugendliche arbeiten jeweils an einem eigenen Laptop.](https://photos.gutwe.in/uploads/medium/b7/30/6de7a8107355b99a6c47c0fd2feb.jpg){loading="lazy"}
](https://photos.gutwe.in/gallery#mA__hehH9d3zadCgwUv4JSWJ){target="_blank" .gallery-preview .horizontal .two style="--a1: 35%; --a2: 25%;" aria-label="Fotogalerie zur Veranstaltung am 24. Juni 2023"}

{% endtimeline %}
{% timeline "16" %}

### Mai 2023 <time datetime="2023-05-06">06. Mai 2023</time>

Dank der [VR-Bank Würzburg](https://www.vr-bank-wuerzburg.de/){target="_blank" rel="noopener noreferrer"} haben wir die Möglichkeit, in den Räumlichkeiten des ehem. Mozartgymnasiums ein CoderDojo abzuhalten.

[
  ![Eine Gruppe sitzt mit ein paar Laptops und Telefoniegeräten an einem Tisch.](https://photos.gutwe.in/uploads/medium/8c/d6/62b9ffce3331d83f3805f38bb25c.jpg){loading="lazy"}
  ![An einer langen Tafel sitzen Teilnehmende, jeweils mit einem eigenen Laptop, und spielen Minecraft. Im Hintergrund ist ein Beamer-Bild zu sehen, dass die Fortschritte bei den digitalen Schaltungen im Spiel zeigt.](https://photos.gutwe.in/uploads/medium/ed/d5/b4b61e291bcc539e34c7df5c9422.jpg){loading="lazy"}
](https://photos.gutwe.in/gallery#NaNRMe0guKXm_pLhidY2M5Yv){target="_blank" .gallery-preview .horizontal .two style="--a1: 50%; --a2: 40%;" aria-label="Fotogalerie zur Veranstaltung am 06. Mai 2023"}

{% endtimeline %}
{% timeline "15" %}

### April 2023 <time datetime="2023-05-06">13. April 2023</time>

Zusammen mit dem [Naturerlebniszentrum Rhön](https://www.nez-rhoen.de/){target="_blank" rel="noopener noreferrer"} bieten wir eine kleine CoderDojo-Veranstaltung im Rahmen des [Osterferienprogramms Bad Kissingen](https://www.badkissingen.de/m_128696){target="_blank" rel="noopener noreferrer"} an.

{% endtimeline %}
{% timeline "14" %}

### März 2023 <time datetime="2023-03-11">11. März 2023</time>

Wir sind zurück aus der Corona-Pause!
Die [iWelt](https://www.iwelt.de/){target="_blank" rel="noopener noreferrer"} heißt uns wieder in Eibelstadt willkommen.

[
  ![Der Aufbau eines 3D-Druckers wird begutachtet. Im Hintergrund ist ein Tisch des Hardware-Workshops mit auseinandergebauten Computern.](https://photos.gutwe.in/uploads/small/ed/47/d8d00a9f6c20ff93e142ee5078c9.jpg){loading="lazy"}
  ![Das Foto zeigt einen Raum voller iMacs, auf denen jeweils ein Programm zum Entwickeln für LEGO Mindstorms läuft.](https://photos.gutwe.in/uploads/small/4d/26/596462b084adffce102d3916dd82.JPG){loading="lazy"}
  ![Zwei Teilnehmer zeigen an einem großen Bildschirm ihr entwickeltes Scratch-Spiel. Es ist eine Rakete zu sehen, die durch das Weltall fliegt. Im Hintergrund stehen die anderen Teilnehmenden, welche die Präsentation verfolgen.](https://photos.gutwe.in/uploads/medium/87/0d/9c36b8ab0146c8d98b9df299b108.JPG){loading="lazy"}
](https://photos.gutwe.in/#hgECjBBlTHMOyuLI5fxURFz2){target="_blank" .gallery-preview .flip style="--x1: 40%; --y1: 55%; --x2: 60%; --y2: 30%;" aria-label="Fotogalerie zur Veranstaltung am 11. März 2023"}

{% endtimeline %}
{% timeline "13" %}

### Januar 2021 <time datetime="2021-01-16">16. Januar 2021</time>

Zum zweiten Mal veranstalten wir ein kleines Online-Event.
Abgesehen vom Veranstaltungsort ändert sich also nicht viel. Wie üblich könnt ihr jederzeit die Gruppen wechseln und euch alles ansehen.

{% endtimeline %}
{% timeline "12" %}

### Meet and Code + WueWW 2020 <time datetime="2020-10-18">18. Oktober 2020</time>

Anlässlich des diesjährigen [Meet and Code](https://meet-and-code.org/gb/en/award2020){target="_blank" rel="noopener noreferrer"} und der [Würzburg Web Week](https://wueww.de/rueckblick-2020/){target="_blank" rel="noopener noreferrer"} veranstalten wir zum ersten Mal ein Online-Event.
Wir bieten wie gewohnt verschiedene Themen in Gruppen anbieten, diesmal aber in virtuellen Online-Räumen.

Gleichzeitig spielen wir spezielle Event-Challenges, die es zu lösen gilt.
Der/die Sieger/-in wird natürlich auch mit einem Preis belohnt!

{% endtimeline %}
{% timeline "11" %}

### November 2019 <time datetime="2019-11-30">30. November 2019</time>

Zum zweiten Mal sind wir in der Stadtbücherei Würzburg mit freundlicher Unterstützung vom Stadtjugendring.

[
  ![Wir lassen im Innenraum der Stadtbücherei zwei Drohnen fliegen.](https://photos.gutwe.in/uploads/small/68189acddfbcb196b6fb8c4dfacae96c.JPG){loading="lazy"}
  ![Aus kleinen Bauklötzen bauen Teilnehmende eigene Level eines Jump-n-Run-Spiels, welches sie anschließend am Tablet ausprobieren können.](https://photos.gutwe.in/uploads/small/d9cf1036f9deefb20e1c3a3d840bf454.JPG){loading="lazy"}
  ![Das Foto zeigt einen gut besuchten Veranstaltungsraum mit kleineren Tischgruppen, auf denen jeweils ein paar Laptops aufgebaut sind.](https://photos.gutwe.in/uploads/medium/dd3bd2f936c889bcadf2a149c42684fa.JPG){loading="lazy"}
](https://photos.gutwe.in/#6aQWCEzvZtj9h62EXPYHFN_H){target="_blank" .gallery-preview .flip style="--x1: 50%; --y1: 45%; --x2: 60%; --y2: 70%;" aria-label="Fotogalerie zur Veranstaltung am 27. Juli 2019"}

{% endtimeline %}
{% timeline "10" %}

### EU Code Week 2019 <time datetime="2019-10-18">18. Oktober 2019</time>

Im Rahmen der [EU Code Week](https://codeweek.eu/){target="_blank" rel="noopener noreferrer"} veranstalten wir ein Nachmittags-Dojo im kleinen Rahmen beim [ZDI Mainfranken](https://www.zdi-mainfranken.de/index.html){target="_blank" rel="noopener noreferrer"}.

{% endtimeline %}
{% timeline "09" %}

### Juli 2019 <time datetime="2019-07-27">27. Juli 2019</time>

Zum zweiten Mal sind wir bei der [iWelt](https://www.iwelt.de/){target="_blank" rel="noopener noreferrer"}!

[
  ![Ein Teilnehmer sitzt vor einem abgestürzten Windows-XP-Laptop.](https://photos.gutwe.in/uploads/small/cc57aa0f78919a8332408103ba18c7d3.jpg){loading="lazy"}
  ![Auf einem Tisch voller Computerteile steht ein fast fertig zusammengebauter Rechner.](https://photos.gutwe.in/uploads/small/437fcd2e2b7ba90cf3d4299c5892ca9b.jpg){loading="lazy"}
  ![In einem großen Konferenzraum finden parallel viele kleine Angebote statt.](https://photos.gutwe.in/uploads/small/d6355281c8de91eed456d8984e35f74c.jpg){loading="lazy"}
](https://photos.gutwe.in/#5FIQxfdnqx4Ft1PLcDStmhUU){target="_blank" .gallery-preview style="--x1: 45%; --y1: 50%; --x2: 25%; --y2: 35%;" aria-label="Fotogalerie zur Veranstaltung am 27. Juli 2019"}

{% endtimeline %}
{% timeline "08" %}

### Juni 2019 <time datetime="2019-06-01">1. Juni 2019</time>

Zweites CoderDojo beim [ZDI Mainfranken](https://www.zdi-mainfranken.de/index.html){target="_blank" rel="noopener noreferrer"}.

[
  ![Ein Vortragender erklärt Interessierten etwas über den Beruf eines Fachinformatikers.](https://photos.gutwe.in/uploads/small/c13faae06923f6a973ec1e35861f79c9.jpg){loading="lazy"}
  ![Eine Gruppe steht um einen Tisch mit Laptops und lauscht einer Erklärung.](https://photos.gutwe.in/uploads/medium/0fff2cb998c9a92e970f4fbeaf375996.jpg){loading="lazy"}
](https://photos.gutwe.in/#EX6IbU3-y7Z5seIK-1VRe8b4){target="_blank" .gallery-preview .horizontal .two style="--a1: 45%; --a2: 30%;" aria-label="Fotogalerie zur Veranstaltung am 1. Juni 2019"}

{% endtimeline %}
{% timeline "07" %}

### WueWW 2019 <time datetime="2019-04-06">6. April 2019</time>

Zum ersten Mal sind wir bei der [Würzburg Web Week](https://wueww.de/rueckblick-2019/){target="_blank" rel="noopener noreferrer"} dabei, mit tatkräftiger Unterstützung von [Mayflower](https://mayflower.de/){target="_blank" rel="noopener noreferrer"}!

[
  ![Eine Reihe Laptops mit dem Spiel Minecraft sind zu sehen. Hiermit werden die Grundlagen elektronischer Schaltungen erlernt.](https://photos.gutwe.in/uploads/small/6c40f50bd34ced65494dceec536ab443.jpg){loading="lazy"}
  ![Teilnehmende lernen an der Tafel Binärcode.](https://photos.gutwe.in/uploads/small/a3933c477e4a81658ebd5d6248e4ac4c.jpg){loading="lazy"}
  ![Ein Roboter bemalt mit einem schwarzen Stift ein Ei.](https://photos.gutwe.in/uploads/small/0d25ba63c2764008fd7520c1efc987c4.jpg){loading="lazy"}
](https://photos.gutwe.in/#L2wzlFqG0aJ8lLUQjNU4Ir_M){target="_blank" .gallery-preview .flip style="--x1: 50%; --y1: 55%; --x2: 25%; --y2: 60%;" aria-label="Fotogalerie zur Veranstaltung am 6. April 2019"}

{% endtimeline %}
{% timeline "06" %}

### Januar 2019 <time datetime="2019-01-26">26. Januar 2019</time>

Das erste Mal sind wir beim [ZDI Mainfranken](https://www.zdi-mainfranken.de/index.html){target="_blank" rel="noopener noreferrer"} zu Gast.


[
  ![Teilnehmende arbeiten gemeinsam an einer reihe Tafeln.](https://photos.gutwe.in/uploads/medium/d61963aa2f4334dfe5363e572a31bde3.jpg){loading="lazy"}
  ![Eine Gruppe bastelt mit zwei Laptops an einem Mikrocontroller.](https://photos.gutwe.in/uploads/medium/e79044c439d385ce87b9bf0a3ce7aa2f.jpg){loading="lazy"}
](https://photos.gutwe.in/#nWP5b-r7kHGwSel7NUj0sbee){target="_blank" .gallery-preview .horizontal .two style="--a1: 55%; --a2: 75%;" aria-label="Fotogalerie zur Veranstaltung am 26. Januar 2019"}

{% endtimeline %}
{% timeline "05" %}

### Winter 2018 <time datetime="2018-12-01">1. Dezember 2018</time>

Zum dritten Mal sind wir mit Unterstützung der Studierenden im Masterstudiengang Informationssysteme an der THWS in Würzburg.

[
  ![Eine Gruppe aus Teilnehmenden und Mentoren arbeitet an der Programmierung von Mikrocontrollern.](https://photos.gutwe.in/uploads/small/510f3489037b83eeb265bf7ac078f14c.jpg){loading="lazy"}
  ![Eine Gruppe beschäftigt sich mit den Komponenten eines Computers.](https://photos.gutwe.in/uploads/small/2296312a9fdcc9eaa462e812919ff7ce.jpg){loading="lazy"}
  ![Ein Mitarbeiter des CoderDojo-Teams schiebt einen Wagen voller Pizzakartons in den Raum.](https://photos.gutwe.in/uploads/small/52a245d07f6efbf97abe41ab784e4c41.jpg){loading="lazy"}
](https://photos.gutwe.in/#GqsUyLjz0xt_E3Wgqtwvsc4C){target="_blank" .gallery-preview .flip style="--x1: 55%; --y1: 50%; --x2: 70%; --y2: 65%;" aria-label="Fotogalerie zur Veranstaltung am 1. Dezember 2018"}

{% endtimeline %}
{% timeline "04" %}

### Herbst 2018 <time datetime="2018-10-27">27. Oktober 2018</time>

Das erste CoderDojo in der [Stadtbücherei Würzburg](https://blog-stadtbuecherei-wuerzburg.de/category/makerspace/){target="_blank" rel="noopener noreferrer"}.

[
  ![Ein Teilnehmer befestigt eine Festplatte in einem PC-Gehäuse.](https://photos.gutwe.in/uploads/small/e4f81006c72229260384c9d1a3e7556d.jpg){loading="lazy"}
  ![Das Bild zeigt eine paar Knetmasse-Kugeln, die zu Controllern für ein Videospiel verkabelt wurden.](https://photos.gutwe.in/uploads/small/395efa752ba21b7c01b6588538ecf866.jpg){loading="lazy"}
  ![Eine Gruppe baut sich mit Klemmbausteinen und bunter Folie ein Set für ihr Stopmotion-Videoprojekt.](https://photos.gutwe.in/uploads/small/47b1344cdd3870c4653a259047952da1.jpg){loading="lazy"}
](https://photos.gutwe.in/#vGUfdmGfjIE2M5ZHy946ZRXt){target="_blank" .gallery-preview .horizontal style="--a2: 20%; --b2: 60%;" aria-label="Fotogalerie zur Veranstaltung am 27. Oktober 2018"}

{% endtimeline %}
{% timeline "03" %}

### Sommer 2018 <time datetime="2018-08-04">4. August 2018</time>

Das erste CoderDojo bei der [iWelt](https://www.iwelt.de/){target="_blank" rel="noopener noreferrer"}.

[
  ![Eine Gruppe wartet auf die Ergebnisse des 3D-Druckers.](https://photos.gutwe.in/uploads/medium/a0af32b0d194534073838bc7f792ab48.jpg){loading="lazy"}
  ![Ein Stapel Namensschilder wartet darauf, abgeholt zu werden.](https://photos.gutwe.in/uploads/small/16d23e9e18e417e29bf280ae7d1ee467.jpg){loading="lazy"}
  ![Teilnehmende am LEGO-Workshop duellieren sich mit ihren Robotern.](https://photos.gutwe.in/uploads/small/909a67293c45757b7689e202e8c32301.jpg){loading="lazy"}
](https://photos.gutwe.in/#zVbGX0LKmcvquJY2pnITspHF){target="_blank" .gallery-preview style="--x1: 40%; --y1: 45%; --x2: 30%; --y2: 30%;" aria-label="Fotogalerie zur Veranstaltung am 4. August 2018"}

{% endtimeline %}
{% timeline "02" %}

### Winter 2017 <time datetime="2017-12-02">2. Dezember 2017</time>

Das zweite CoderDojo in Würzburg, wieder bei der THWS.

[
  ![Ein Mentor erklärt Teilnehmenden an einer Leinwand die Programmiersprache Scratch.](https://photos.gutwe.in/uploads/medium/c1f0beec7e583342c86d19fb600afc93.jpg){loading="lazy"}
  ![Teilnehmende sitzen sich an einer langen Tafel gegenüber, die meisten haben einen Laptop dabei.](https://photos.gutwe.in/uploads/small/64c176028137b595901ae6ff332931e8.jpg){loading="lazy"}
  ![Das Foto zeigt das Pizza-Buffet zu Mittag.](https://photos.gutwe.in/uploads/small/7631ba1b10c3b9913a99b855d82e9774.jpg){loading="lazy"}
](https://photos.gutwe.in/#tdLCbgFptu_dQT9vyKS5Nm4P){target="_blank" .gallery-preview style="--x1: 45%; --y1: 55%; --x2: 35%; --y2: 70%;" aria-label="Fotogalerie zur Veranstaltung am 2. Dezember 2017"}

{% endtimeline %}
{% timeline "01" %}

### Frühjahr 2017 <time datetime="2017-03-25">25. März 2017</time>

Erstes CoderDojo in Würzburg &ndash; wir sind zu Gast in den Räumlichkeiten der THWS.

[
  ![Die Veranstaltung ist mit knapp 30 Personen im Raum gut besucht.](https://photos.gutwe.in/uploads/medium/a81901204551f6d60430542457f99415.jpg){loading="lazy"}
  ![Eine Person arbeitet mit einem Werkzeugkasten mit Widerständen, LEDs und anderen elektronischen bauteilen.](https://photos.gutwe.in/uploads/small/7f02dfac22a0f9ba1821fb28a5c1fc85.jpg){loading="lazy"}
  ![Das Foto zeigt den RoboCup, einen Hindernislauf, den Teilnehmende selbst durchlaufen müssen, indem sie sich mit Stift und Papier einen Weg "programmieren".](https://photos.gutwe.in/uploads/small/1eef83027c6e95b5abfbdfc514eb9301.jpg){loading="lazy"}
](https://photos.gutwe.in/#a50fFm28Vy-oPhacpSAJHaVY){target="_blank" .gallery-preview style="--x2: 70%;" aria-label="Fotogalerie zur Veranstaltung am 25. März 2017"}

{% endtimeline %}
