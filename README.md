# Angestöpselt im Web

Hallo! :wave:

Was du hier siehst, ist der Code für die wichtigen Internetauftritte von Angestöpselt.
Dazu zählen in erster Linie

- unsere Homepage auf [angestoepselt.de](https://angestoepselt.de/) und
- die Seite zum CoderDojo-Projekt auf [coderdojo-wue.de](https://coderdojo-wue.de/).

Gebaut ist das Ganze mit [Eleventy](https://www.11ty.dev/).
Eleventy ist ein Programm, das aus einer Menge [Markdown](https://de.wikipedia.org/wiki/Markdown)-Dateien eine lauffähige Homepage baut.
Alle erwähnten Seiten verwenden die gleiche Basis, unterscheiden sich allerdings in ihrem Inhalt.

### Ich möchte an der Homepage mitarbeiten.

Wir haben [hier](https://codeberg.org/angestoepselt/homepage/src/branch/main/CONTRIBUTING.md) eine etwas umfassendere Dokumentation zum Mitarbeiten.
Falls du es eilig hast, hier sind die Grundlagen:

1. Node in einer einigermaßen aktuellen Version installieren
2. Das Projekt auschecken, dann `npm install`
3. `npm run build:styles`
4. `SITE=angestoepselt npm run dev:site`<sup>1</sup>
    - Du solltest einen lokalen Testserver bekommen, der auf Änderungen reagiert. Formulare funktionieren hier nicht.
    - Im Ordner **dist/** liegen die fertigen Dateien für den Webserver.
5. Änderungen in einem PR auf den `main`-Zweig einreichen

<small>1: Wähle für die Umgebungsvariable den Namen des Ordners unterhalb von **sites/** für die Seite, die du bearbeiten möchtest.</small>

### Ich habe einen Fehler entdeckt oder habe einen Verbesserungsvorschlag.

Falls du selbst ein Entwickler bist, kannst du auf hier auf Codeberg einen Fork des Projekts erstellen und einen Pull-Request einreichen.
Danke!

Falls nicht, schreibe uns gerne an [info@angestoepselt.de](mailto:info@angestoepselt.de) oder über unser [Kontaktformular](https://angestoepselt.de/kontakt/#andere-anliegen).

### Ich interessiere mich für den Code.

Dann bist du hier genau richtig.
Sieh dich ruhig ein wenig um!

Das Projekt ist [MIT-Lizenziert](https://codeberg.org/angestoepselt/homepage/src/branch/main/LICENSE).
