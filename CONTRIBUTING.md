# Entwickler-Dokumentation

## Struktur

Das Projekt läuft mit dem Static-Site-Generator [Eleventy](https://www.11ty.dev/).
Anders als bei anderen Eleventy-Projekten liegt hier aber nicht nur einen Internetauftritt, sondern mehrere.
Alle teilen sich gemeinsames CSS, CGI-Skripte und noch ein paar andere Geschichten.
Sie unterscheiden sich nur durch die konkreten Inhalte.

Du solltest nach dem Auschecken (oder in der Online-GUI) folgende Ordnerstruktur vorfinden:

- **assets/** – Hilfsdateien für alle Auftritte. Dieser Ordner ist später unter `/assets` per HTTP verfügbar. Alles, was in diesem Ordner liegt, wird 1:1 kopiert.
- **cgi-bin/** – CGI-Skripte liegen hier. Diese sind zwar für alle Auftritte verfügbar, sind aber nicht überall genutzt. Diese Skripte werden auch nicht direkt von außen aufgerufen. Stattdessen schreibt der HTTP-Server relevante Anfragen um.
- **includes/** – Layout und Template-Dateien. Siehe dazu die [Eleventy-Dokumentation](https://www.11ty.dev/docs/config/#directory-for-includes).
- **nix/** – [Nix](https://nixos.org/)-spezifisches. Wahrscheinlich musst du hier nichts tun.
- **playground/** – Das ist die Spielwiese. Wenn du irgendetwas ausprobieren möchtest, was grob zum Projekt passt, aber sonst keinen Platz in der Ordnerstruktur hat, leg es hier ab.
- **sites/** – Hier bekommt jeder Internetauftritt einen eigenen Unterordner.
  - **&lt;name&gt;/**
    - **\_static/** – Statischer Inhalt, der nur für diesen Auftritt gedacht ist. Inhalte in diesem Ordner werden ohne Änderungen später live verfügbar sein.
      - **assets/** – Hilfsdateien für diesen Auftritt. Diese sind später unter `/assets` per HTTP verfügbar.
    - **\_data/** – [Globale Datendateien](https://www.11ty.dev/docs/data-global/) für Eleventy. Hier sind Metadaten zu dem Internetauftritt gespeichert.
    - **\_images/** – Hier sind Bilder gespeichert, die vor der Liveschaltung nochmals angepasst werden. Dazu gehört Beispiel die automatisierte Konvertierung in verschiedene Formate.
    - **httpd.conf** – Konfigurationsdatei für den HTTP-Server [lighttpd](https://redmine.lighttpd.net/projects/lighttpd/wiki#Documentation). Diese bestimmt die finale Auslieferung der Seite
    - _Alles andere_ in diesem Ordner sind Inhalte für die eigentliche Homepage.
- **styles/** – [SCSS](https://sass-lang.com/)-Dateien. Alles, was hier nicht mit einem Unterstrich beginnt, ist später in `/assets/css` verfügbar.
- **.eleventy.js** – [Eleventy-Konfigurationsdatei](https://www.11ty.dev/docs/config/). Unsere Config ist so aufgesetzt, dass die entsprechend eingestellte Seite (siehe unten) geladen wird.
- **.eleventyignore** – Ignore-Datei für Eleventy. Diese enthält Pfade, die beim Bauen der Seite ignoriert werden.

## Lokale Entwicklung

Um das Projekt lokal zu bauen, musst du zunächst [Node](https://nodejs.org) sowie [git](https://git-scm.com/) installieren.
Dann kannst du das Repository klonen (stelle sicher, dass ein SSH Schlüssel in deinem Codeberg-Account [hinterlegt](https://docs.codeberg.org/security/ssh-key/) ist):

```shell
git clone git@codeberg.org:angestoepselt/homepage.git
```

Dann führst du im Ordner des Projekts (`cd homepage`) folgenden Befehl aus:

```shell
npm install
```

Das installiert sämtliche zur Entwicklung benötigten Abhängigkeiten.
Evtl. dauert das ein paar Minuten–hol dir ruhig einen Kaffee!
Du kannst `npm install` ruhig alle paar Wochen mal ausführen, um auf dem aktuellen Stand zu bleiben.

Anschließend kompilierst du die CSS-Dateien, damit du später nicht nur eine weiße Textwand zu sehen bekommst:

```shell
npm run build:styles
```

Wenn du allerdings explizit an den CSS-Dateien arbeiten willst, solltest du stattdessen `dev:styles` ausführen.
Solange der Prozess läuft, werden die Stylesheets stets neu kompiliert, wenn du sie veränderst.

Jetzt kannst du die eigentliche Webseite starten.
Dazu musst du zunächst den Namen des Unterordners von **sites/** notieren, der zu der Webseite passt, an der du arbeiten möchtest.
Diesen Namen schreibst du in die Umgebungsvariable `SITE`.
Anschließend kannst du den Server starten.
Hier beispielhaft für die Angestöpselt-Seite:

```shell
# Unter Linux / Mac:
export SITE=angestoepselt
# Oder in der PowerShell (Windows):
$Env:SITE='angestoepselt'

# Und dann im Anschluss:
npm run dev:site
```

## Deployment

Der `main`-Zweig wird automatisch durch unseren [CI-Server](https://drone.z31.it) deployt.
Das bedeutet: **alles, was auf `main` ist, ist live**!
Hier sind die jeweiligen URLs:

- Angestöpselt: [angestoepselt.de](https://angestoepselt.de/)
- CoderDojo: [coderdojo-wue.de](https://coderdojo-wue.de/)

Das Deployment funktioniert via [Portainer](https://portainer.pub.z31.it).
Verwendet wird dazu die [docker-compose.yml](https://codeberg.org/angestoepselt/homepage/src/branch/main/docker-compose.yml) aus diesem Repository.

### Konfiguration

Damit das Docker-Deployment (auch lokal) funktioniert, müssen ein paar Umgebungsvariablen gesetzt sein.
Hier sind sie, zusammen mit ihren Standards:

- `ZAMMAD_URL=https://ticket.z31.it` – Basis-URL der Zammad-Instanz, die durch die Kontaktformulare befüllt wird.
- `ZAMMAD_GROUP=""` – Ist diese Variable gesetzt (das ist sie in der Staging-Umgebung), landen alle Kontaktformular-Einträge in dieser Zammad-Gruppe. Standardmäßig (wenn dieser Wert leer ist) werden sie je nach Thema in die korrekte Gruppe einsortiert.
- `ZAMMAD_TOKEN` – API-Schlüssel zur Zammad-Instanz. Dieser Schlüssel muss die `ticket.agent`-Berechtigung haben.
- `HCAPTCHA_SITE_KEY` – Quasi-öffentlicher Schlüssel zum [Einbinden von hCaptcha](https://docs.hcaptcha.com/#add-the-hcaptcha-widget-to-your-webpage).
- `HCAPTCHA_SECRET_KEY` – Geheimer Schlüssel für die [Serverseitige Kontrolle der hCaptcha-Antworten](https://docs.hcaptcha.com/#verify-the-user-response-server-side).

Die letzten drei sind pflicht.

Wenn du das Image lokal bauen möchtest, musst du das [Argument](https://docs.docker.com/build/guide/build-args/) `SITE` mitgeben, genau wie bei der lokalen Entwicklung.

## Staging-Umgebung

Wir haben für die beiden großen Seiten jeweils eine Staging-Umgebung:

- Angestöpselt: [csw.stage.angestoepselt.de](https://csw.stage.angestoepselt.de/)
- CoderDojo: [coderdojo.stage.angestoepselt.de](https://coderdojo.stage.angestoepselt.de/)

Die Staging-Umgebung **zeigt immer auf die zeitlich neuste Änderung aus allen nicht-`main`-Zweigen**.
Wenn mehrere Personen gleichzeitig an unterschiedlichen Zweigen arbeiten wird die Staging-Umgebung immer das zeigen, was als letztes gepusht wurde.

Natürlich ist das nicht ganz ideal, aber für unsere aktuelle Entwicklungsfrequenz dürfte das reichen.

## Workflow für Änderungen

Wenn du deine Änderungen gerne online stellen möchtest, musst du einen [Pull-Request](https://docs.codeberg.org/collaborating/pull-requests-and-git-flow/) einreichen.
Ein Pull-Request bündelt deine Änderungen, sodass sie jemand vom Homepage-Team anschauen, genehmigen und einpflegen kann.

Wenn du deine Änderungen über das Webinterface von Codeberg erstellst, wählst du beim Bearbeiten einer Datei im *Änderungen Commiten*-Dialog die Option "Einen neuen Branch für diesen Commit erstellen und einen Pull Request starten".
Wenn du anschließend in den Pull-Request noch eine weitere Änderung aufnehmen möchtest, sucher [hier](https://codeberg.org/angestoepselt/homepage/branches) deinen entsprechenden Branch und wähle ab der zweiten Bearbeitung in dem erwähnten Dialog "Direkt in den Branch `...` einchecken".

Wenn du Mitglied des [Homepage](https://codeberg.org/org/angestoepselt/teams/homepage)-Teams auf Codeberg bist, kannst du ohne Forken direkt auf unserem Repository arbeiten.
Dann kannst du auch die Staging-Umgebung verwenden (siehe oben).
Falls du gerne Mitglied werden willst, melde dich bitte bei [Matthias](https://codeberg.org/matti) oder [Yannik](https://codeberg.org/yrd).
Du kannst auch hier im Projekt ein Issue erstellen.

Du kannst so viele Pull-Requests gleichzeitig erstellen wie du magst (musst also nicht warten, bis der erste genehmigt ist).
Bitte erstelle einen neuen PR für jedes "Thema" das du anschneidest.
Aktuell kümmern sich [Matthias](https://codeberg.org/matti) und [Yannik](https://codeberg.org/yrd) um das Zusammenführen der PRs.
