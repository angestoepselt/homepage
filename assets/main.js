for (const tabLinkElement of document.querySelectorAll(
	'h2 + .tabs-widget > a[href]'
)) {
	const titleElement = tabLinkElement.parentElement.previousElementSibling;
	tabLinkElement.addEventListener('click', (event) => {
		event.preventDefault();
		const hash = tabLinkElement.getAttribute('href');
		if (history.replaceState) {
			// This is a weird hack to make sure that the :target selectors actually
			// get updated:
			// https://github.com/whatwg/html/issues/639#issuecomment-252716663
			history.replaceState(null, '', hash);
			history.pushState(null, '', hash);
			history.back();
			titleElement.scrollIntoView(true);
		} else {
			location.hash = hash;
		}
	});
}
